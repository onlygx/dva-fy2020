package com.ow.dva;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.StreamProgress;
import cn.hutool.core.lang.Console;
import cn.hutool.http.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述 :
 * 包名 : com.ow.dva
 * 作者 : GaoXiang
 * 时间 : 18-11-19
 * 版本 : V1.0
 */
public class DownTest {

    private static Logger logger = LoggerFactory.getLogger(DownTest.class);

    private static String url = "http://jiaotong.baidu.com/guidingscreen/screen/image?id=1808";

    public static void main(String[] args) {
        //hutoolDown(url);
    }

    public static void hutoolDown(String url){
        long size = HttpUtil.downloadFile(url, FileUtil.file("/home/gaoxiang/下载/","files.jpg"),new StreamProgress(){

            @Override
            public void start() {
                logger.info("开始下载。。。。");
            }

            @Override
            public void progress(long progressSize) {
                logger.info("已下载：{}", FileUtil.readableFileSize(progressSize));
            }

            @Override
            public void finish() {
                logger.info("下载完成！");
            }
        });
        System.out.println("Download size: " + size);
    }
}

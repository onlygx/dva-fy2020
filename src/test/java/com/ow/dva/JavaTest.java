package com.ow.dva;


import cn.hutool.crypto.digest.MD5;

/**
 * 描述 :
 * 包名 : com.ow.dva
 * 作者 : GaoXiang
 * 时间 : 18-11-16
 * 版本 : V1.0
 */
public class JavaTest {

    public static void main(String[] args) {
        System.out.println(new MD5().digestHex("123456"));
    }
}

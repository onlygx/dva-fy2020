package com.ow.dva;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * <p>
 * 描述 :
 * </p>
 * <p>
 * 包名 : com.ow.dva
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-4-28
 */
public class LinuxExec {

    static Logger logger = LoggerFactory.getLogger("test");

    public static void main(String[] args) {

        try {
            String[] cmd = new String[] { "/home/gaoxiang/logs/","ls -l" };
            Process ps = Runtime.getRuntime().exec(cmd);

            BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            String result = sb.toString();

            logger.info(result);

        } catch (Exception e) {
            logger.error("----error-----");
            e.printStackTrace();

        }
    }
}

package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 * @version V1.0
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    /**
    * 分页获取${table.comment!}数据列表信息。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<${entity}> page(DvaPage<${entity}> page, Map<${"String"},Object> param);
}
</#if>

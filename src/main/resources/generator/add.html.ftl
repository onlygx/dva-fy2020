<!--
  ${table.comment!}添加页面
  User: ${author}
  Date: ${date}
-->
<!DOCTYPE html>
<html lang="zh" xmlns:th="http://www.thymeleaf.org" >

<!-- BEGIN FORM-->
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red-sunglo bold uppercase">${table.comment!}添加</span>
            <span class="caption-helper">添加一条${table.comment!}数据</span>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="save-module">
            <div class="form-body">

                <#list table.fields as field>
                    <!-- ${field.comment} 字段信息-->
                    <#if field.propertyType == "Date">
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-7">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="text" class="form-control bs-date" required name="${field.propertyName}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <#elseif field.propertyName == "id" >
                    <#elseif field.propertyType == "Integer">
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-7">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="number" class="form-control" required digits="true" name="${field.propertyName}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <#else>
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-7">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="text" class="form-control" required name="${field.propertyName}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </#if>
                </#list>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-7">
                                <button type="button" class="btn green" onclick="save();">保存</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>

<script>

    $(function(){
        //初始化页面
        initPage();
    });

    function save(){
        let param = tools.formParams("save-module");
        if(tools.valid("save-module")){
            tools.post("/${entity?uncap_first}/save",param,function(data){
                if(data.success){
                    layer.msg('保存成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_addModule);
                    });
                }else{
                    tools.errorTip(data);
                }
            });
        }
    }

</script>
</html>
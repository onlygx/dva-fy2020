package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
<#if swagger2>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
</#if>
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 * @version V1.0
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
<#if swagger2>
@Api(tags = "${table.comment!} 相关控制器")
</#if>
<#--@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")-->
<#--@RequestMapping("/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")-->
@RequestMapping("/<#if controllerMappingHyphenStyle??>${entity?uncap_first}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    /**
    * 分页获取${table.comment!}数据列表信息
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "分页获取",notes = "批量获取${table.comment!}信息列表，分页，可携带条件")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<${entity}> dvaPage, @RequestParam Map<${"String"}, Object> param, Model model){

        DvaPage<${entity}> page = ${entity?uncap_first}Service.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "${entity?uncap_first}/list";
    }

    /**
    * 进入${table.comment!}添加页面。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "进入添加页面",notes = "进入${table.comment!}添加页面")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:add")
    @GetMapping(value = "/add")
    public String add(){

        return "${entity?uncap_first}/add";
    }

    /**
    * 添加${table.comment!}信息。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "添加",notes = "添加单个${table.comment!}信息")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(${entity} ${entity?uncap_first}){

        return ${entity?uncap_first}Service.save(${entity?uncap_first}) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入${table.comment!}编辑页面。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "进入编辑页面",notes = "进入${table.comment!}编辑页面")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",${entity?uncap_first}Service.getById(id));

        return "${entity?uncap_first}/edit";
    }

    /**
    * 修改${table.comment!}信息。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "修改",notes = "修改单个${table.comment!}信息")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(${entity} ${entity?uncap_first}){

        return ${entity?uncap_first}Service.updateById(${entity?uncap_first}) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个${table.comment!}信息。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "删除",notes = "删除单个${table.comment!}信息")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的${table.comment!}ID") String id){

        return ${entity?uncap_first}Service.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个${table.comment!}信息。
    * 创建时间：${date} ${author}
    * 修改时间：${date} ${author} 加注释
    * 修改时间：
    */
    <#if swagger2>
    @ApiOperation(value = "批量删除",notes = "批量删除多个${table.comment!}信息")
    </#if>
    @RequiresPermissions("${entity?uncap_first}:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的${table.comment!}ID List") @RequestParam List<String> ids){

        return ${entity?uncap_first}Service.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    ${table.serviceName} ${entity?uncap_first}Service;
}
</#if>

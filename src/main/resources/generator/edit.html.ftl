<!--
  ${table.comment!}编辑页面
  User: ${author}
  Date: ${date}
-->
<!DOCTYPE html>
<html lang="zh" xmlns:th="http://www.thymeleaf.org" >

<!-- BEGIN FORM-->
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red-sunglo bold uppercase">${table.comment!}编辑</span>
            <span class="caption-helper">编辑一条${table.comment!}数据</span>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="#" class="form-horizontal" id="edit-module">
            <div class="form-body">
                <#list table.fields as field>

                    <!-- ${field.comment} 字段信息-->
                    <#if field.propertyType == "Date">
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-4">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="text" class="form-control bs-date" required name="${field.propertyName}" th:value="${r"${#dates.format(data['"}${field.propertyName}${"'],'yyyy-MM-dd HH:mm:ss')}"}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <#elseif field.propertyType == "Integer">
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-4">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="number" class="form-control" required  name="${field.propertyName}" th:value="${r"${data['"}${field.propertyName}${r"']}"}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <#elseif field.propertyName == "id" >
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-4">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="text" disabled class="form-control" required name="${field.propertyName}" th:value="${r"${data['"}${field.propertyName}${r"']}"}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <#else>
                        <div class="form-group">
                            <label class="col-md-3 control-label">${field.comment}</label>
                            <div class="col-md-4">
                                <div class="input-inline ">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input type="text" class="form-control" required minlength="2" name="${field.propertyName}" th:value="${r"${data['"}${field.propertyName}${r"']}"}" placeholder="${field.comment}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </#if>
                </#list>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn green" onclick="edit();">保存</button>
                                <button type="button" class="btn default" onclick="layer.close(layer_addModule);">取消</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>

<script>

    $(function(){
        //初始化页面
        initPage();
    });

    function edit(){
        let param = tools.formParams("edit-module");
        if(tools.valid("edit-module")){
            tools.post("/${entity?uncap_first}/update",param,function(data){
                if(data.success){
                    layer.msg('修改成功', {icon: 1,time:1000},function(){
                        //刷新列表页面
                        toPage(null);
                        //关闭弹窗
                        layer.close(layer_editModule);
                    });
                }else{
                    tools.errorTip(data);
                }
            });
        }
    }

</script>
</html>
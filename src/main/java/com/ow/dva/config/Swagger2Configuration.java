package com.ow.dva.config;

import com.ow.dva.config.constant.Swagger2Constant;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 描述 : 接口描述配置文件
 * 包名 : com.ow.dva.config
 * 作者 : GaoXiang
 * 时间 : 18-11-19
 * 版本 : V1.0
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    /**
     * swagger2的配置文件，这里可以配置swagger2的一些基本的内容，比如扫描的包等等
     * @return 创建规则
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName(Swagger2Constant.GROUP_NAME_DEFAULT)
                .select()
                //设置basePackage会将包下的所有被@Api标记类的所有方法作为api
                .apis(RequestHandlerSelectors.basePackage(Swagger2Constant.BASE_PACKAGE))
                //只有标记了@ApiOperation的方法才会暴露出给swagger
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     * @return 配置展示信息
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //页面标题
                .title(Swagger2Constant.INFO_TITLE)
                //版本号
                .version(Swagger2Constant.INFO_VERSION)
                //描述
                .description(Swagger2Constant.INFO_DESCRIPTION)
                .build();
    }
}
package com.ow.dva.config.shiro;

import org.apache.shiro.authc.SimpleAuthenticationInfo;

import java.io.Serializable;

public class DvaAuthenticationInfo extends SimpleAuthenticationInfo implements Serializable {

    public DvaAuthenticationInfo(){}

    public DvaAuthenticationInfo(ShiroAccount shiroAccount,String password,String realmName){
        super(shiroAccount,password,realmName);
    }
}

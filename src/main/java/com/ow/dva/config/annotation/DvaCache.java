package com.ow.dva.config.annotation;

import com.ow.dva.config.constant.CacheConstant;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 描述 : 自定义注解 缓存时间60*12分钟
 * 包名 : com.ow.dva.config.annotation
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-10
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Cacheable(value = CacheConstant.CACHE)
public @interface DvaCache {

}

package com.ow.dva.config.constant;

/**
 * <p>
 * 描述 : redis Cache 相关命名 名称和配置的关系参考 RedisCacheManagerConfig 类
 * </p>
 * <p>
 * 包名 : com.ow.dva.config.constant
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-10
 */
public class CacheConstant {

    // 默认的cache ,缓存保留时间 60*12 分钟
    public static final String CACHE = "dvaCache";

    // 1分钟的cache ,缓存保留时间 1 分钟
    public static final String CACHE_1M = "dvaCache1M";

    // 5分钟的cache ,缓存保留时间 5 分钟
    public static final String CACHE_5M = "dvaCache5M";

    // 10分钟的cache ,缓存保留时间 10 分钟
    public static final String CACHE_10M = "dvaCache10M";

    // 30分钟的cache ,缓存保留时间 30 分钟
    public static final String CACHE_30M = "dvaCache30M";

    // 60分钟的cache ,缓存保留时间 60 分钟
    public static final String CACHE_60M = "dvaCache60M";

    // 60分钟的cache ,缓存保留时间 60 分钟
    public static final String CACHE_SHIRO = "dvaCacheShiro";

    /**
     * Spring 使用CGLIB后 类对象命名会加上的内容字符串
     */
    public static final String SPRING_CGLIB_SUFFIX = "$$EnhancerBySpringCGLIB$$";

    /**
     * 缓存key生成拼接时用的分隔符
     */
    public static final String CACHE_KEY_SEPARATOR = ":";
}

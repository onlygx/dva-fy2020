package com.ow.dva.config.constant;

/**
 * 描述 :
 * 包名 : com.ow.dva.config.constant
 * 作者 : GaoXiang
 * 时间 : 19-12-23
 * 版本 : V1.0
 */
public class UserConstant {

    public static final String USERNAME = "userName";

    public static final String ACCOUNT = "account";

    public static final String ACCOUNT_INFO = "accountInfo";

    public static final String ACCOUNT_ADMIN = "accountAdmin";

    public static final String ADMIN = "admin";

    public static final String PERMISSION_LIST = "permissionList";




}

package com.ow.dva.config.constant;

/**
 * <p>
 * 描述 : Swagger2 用到的字符串常量 详情见 Swagger2Config
 * </p>
 * <p>
 * 包名 : com.ow.dva.config.constant
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-14
 */
public class Swagger2Constant {

    /**
     * 接口分组名称
     */
    public static final String GROUP_NAME_DEFAULT = "Api开放接口";

    /**
     * 要扫描的包
     */
    public static final String BASE_PACKAGE = "com.ow.dva";

    /**
     * 展示基础信息:标题
     */
    public static final String INFO_TITLE = "系统服务开放接口信息";

    /**
     * 展示基础信息:标题
     */
    public static final String INFO_VERSION = "1.0";

    /**
     * 展示基础信息:标题
     */
    public static final String INFO_DESCRIPTION = "此文档为动态生成,具备 API 描述和相关接口测试功能.";
}

package com.ow.dva.config.constant;

/**
 * <p>
 * 描述 : 跨域访问相关常量 配置关系参考 CorsConfig 类
 * </p>
 * <p>
 * 包名 : com.ow.dva.config.constant
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-14
 */
public class CorsConstant {

    /**
     * 注册拦截地址
     */
    public static final String REGISTER_URL = "/**";

    /**
     * 允许的来源域名
     */
    public static final String ALLOWED_ORIGIN = "*";

    /**
     * 允许的请求头
     */
    public static final String ALLOWED_HEADER = "*";

    /**
     * 允许的请求方法
     */
    public static final String ALLOWED_METHOD = "*";


}

package com.ow.dva.module.organization.service.impl;

import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.mapper.OrganizationMapper;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-14
 * @version V1.0
 */
@Service
public class OrganizationServiceImpl extends DvaServiceImpl<OrganizationMapper, Organization> implements OrganizationService {

    @DvaCache5M
    @Override
    public DvaPage<Organization> page(DvaPage<Organization> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<Organization> findByParentId(String parentId) {
        return baseMapper.findByParentId(parentId);
    }

    @Override
    public List<Organization> findChildByParentId(String parentId) {
        return baseMapper.findChildByParentId(parentId);
    }

    @Override
    public List<Organization> findAllChildByParentId(String parentId) {
        return baseMapper.findAllChildByParentId(parentId);
    }
}

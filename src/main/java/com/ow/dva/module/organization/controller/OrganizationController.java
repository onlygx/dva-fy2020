package com.ow.dva.module.organization.controller;


import com.ow.dva.module.organization.entity.JsTree;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-14
 * @version V1.0
 */
@Controller
@Api(tags = " 相关控制器")
@RequestMapping("/organization")
public class OrganizationController extends BaseController {

    /**
    * 分页获取数据列表信息
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取信息列表，分页，可携带条件")
    @RequiresPermissions("organization:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Organization> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Organization> page = organizationService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "organization/list";
    }

    @ApiOperation(value = "获取一级组织",notes = "获取全部一级组织")
    @RequiresPermissions("organization:page")
    @GetMapping(value = "/tree")
    public String tree(String parentId, Model model){

        List<Organization> list = organizationService.findByParentId(parentId);
        model.addAttribute("list",list);
        model.addAttribute("parentId",parentId);
        return "organization/tree";
    }
    /**
     * 获取所有下级组织
     * @param parentId 上级ID
     */
    @RequiresPermissions("organization:page")
    @PostMapping(value = "/findByParentId")
    @ResponseBody
    public RT findByParentId(String parentId){
        List<Organization> list = organizationService.findChildByParentId(parentId);
        return list.size() > 0 ? RT.ok(list) : RT.error(RT.ERROR_0101,"暂无数据");
    }

    /**
    * 进入添加页面。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入添加页面")
    @RequiresPermissions("organization:add")
    @GetMapping(value = "/add")
    public String add(){

        return "organization/add";
    }

    /**
    * 添加信息。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个信息")
    @RequiresPermissions("organization:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Organization organization){
        organization.setCreateTime(new Date());
        return organizationService.save(organization) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入编辑页面。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入编辑页面")
    @RequiresPermissions("organization:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",organizationService.getById(id));

        return "organization/edit";
    }

    /**
    * 修改信息。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个信息")
    @RequiresPermissions("organization:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Organization organization){

        return organizationService.updateById(organization) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个信息。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个信息")
    @RequiresPermissions("organization:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的ID") String id){

        return organizationService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个信息。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个信息")
    @RequiresPermissions("organization:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的ID List") @RequestParam List<String> ids){

        return organizationService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    OrganizationService organizationService;
}

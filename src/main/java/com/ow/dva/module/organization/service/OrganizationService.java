package com.ow.dva.module.organization.service;

import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-14
 * @version V1.0
 */
public interface OrganizationService extends DvaService<Organization> {

    /**
    * 分页获取数据列表信息。
    * 创建时间：2020-02-14 高祥
    * 修改时间：2020-02-14 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Organization> page(DvaPage<Organization> page, Map<String,Object> param);

    /**
     * 获取所有下级组织
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     * @param parentId 上级ID
     * @return 带分页的数据集
     */
    List<Organization> findByParentId(String parentId);

    /**
     * 获取指定下级的直属分类 只取一级
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     * @param parentId 上级ID
     * @return 带分页的数据集
     */
    List<Organization> findChildByParentId(String parentId);

    /**
     * 获取所有直属下级分类 取N级
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     * @param parentId 上级ID
     * @return 带分页的数据集
     */
    List<Organization> findAllChildByParentId(String parentId);
}

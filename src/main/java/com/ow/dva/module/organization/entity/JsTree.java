package com.ow.dva.module.organization.entity;

import com.ow.dva.module.base.entity.base.BaseEntity;

import java.io.Serializable;

public class JsTree implements Serializable {

    public JsTree(){};
    public JsTree(String id,String icon,String text){
        this.id = id;
        this.icon = icon;
        this.text = text;
    };

    private String id;
    private String icon;
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

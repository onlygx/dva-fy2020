package com.ow.dva.module.base.controller;

import com.ow.dva.config.constant.CacheConstant;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.base.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.Collection;

/**
 * 描述 : 缓存操作
 * 包名 : com.ow.dva.controller
 * 作者 : GaoXiang
 * 时间 : 18-12-15
 * 版本 : V1.0
 */
@RestController
@RequestMapping(value = "/redis")
@Api(tags = "缓存相关")
public class RedisController extends BaseController {

    /**
     * 缓存数据获取
     * @return 操作结果
     */
    @RequestMapping("/find")
    @ApiOperation(value = "缓存获取",notes = "需要指定key")
    public RT find(String key) {
        String value = redisService.getString(CacheConstant.CACHE,key);
        return RT.ok(value);
    }

    /**
     * 缓存数据添加
     * @return 操作结果
     */
    @RequestMapping("/save")
    @ApiOperation(value = "缓存添加",notes = "需要指定key和value")
    public RT save(String key,String value) {
        redisService.put(CacheConstant.CACHE,key,value);
        return RT.ok("添加成功!");
    }

    /**
     * 缓存数据删除
     * @return 操作结果
     */
    @RequestMapping("/delete")
    @ApiOperation(value = "缓存删除",notes = "需要指定key")
    public RT delete(String key) {
        redisService.delete(CacheConstant.CACHE,key);
        return RT.ok("删除成功!");
    }

    /**
     *  清理缓存 dvaCache5M
     * @return 操作结果
     */
    @RequestMapping("/clear")
    @ApiOperation(value = "缓存删除",notes = "需要指定key")
    public RT clear(String name) {
        redisService.clear(name);
        return RT.ok("清除成功!");
    }

    /**
     * 所有缓存名称列表
     * @return 操作结果
     */
    @RequestMapping("/list")
    @ApiOperation(value = "获取所有缓存名称",notes = "默认获取全部")
    public RT list() {
        Collection<String> cacheList = redisService.listCacheNames();
        return RT.ok("获取成功",cacheList);
    }

    @Resource
    private RedisService redisService;

}

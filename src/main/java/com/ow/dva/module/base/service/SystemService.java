package com.ow.dva.module.base.service;

import java.util.Map;

/**
 * 描述 :
 * 包名 : com.ow.dva.module.base.service
 * 作者 : GaoXiang
 * 时间 : 19-12-20
 * 版本 : V1.0
 */
public interface SystemService {

    public Map<String,String> sysInfo();
}

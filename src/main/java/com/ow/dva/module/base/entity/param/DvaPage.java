package com.ow.dva.module.base.entity.param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;

/**
 * 依托IPage实现类Page，通过继承实现的自定义分页
 * 作者 : 高祥
 * 时间 : 19-12-25
 * 版本 : V1.0
 * @param <T> 数据集类型（List），返回结果集会放在records集合中
 */
@ApiModel("带分页参数的列表数据集")
public class DvaPage<T> extends Page<T> {

    /**
     * 默认构造
     */
    public DvaPage(){
        super();
    }

    /**
     * 分页信息初始化
     * @param current 当前页
     * @param size 每页大小
     */
    public DvaPage(Long current, Long size) {
        super(current, size);
    }

}

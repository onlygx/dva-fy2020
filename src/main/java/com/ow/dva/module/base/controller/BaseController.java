package com.ow.dva.module.base.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述 :
 * 包名 : com.ow.dva.module.base.controller
 * 作者 : GaoXiang
 * 时间 : 19-12-18
 * 版本 : V1.0
 */
public class BaseController{
    //共享日志
    public Logger logger = LoggerFactory.getLogger(this.getClass());
}

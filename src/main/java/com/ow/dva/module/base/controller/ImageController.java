package com.ow.dva.module.base.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.ow.dva.module.base.entity.json.RT;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 描述 : 图片处理 Controller
 * 包名 : com.ow.dva.controller
 * 作者 : GaoXiang
 * 时间 : 18-11-8
 * 版本 : V1.0
 */
@Api(tags = "图片相关")
@Controller
@RequestMapping("/image")
public class ImageController extends BaseController {

    @Value("${dva.static-path}")
    private String staticPath;

    /**
     * 文件上传
     * @param imageFile 图片
     * @return 上传状态
     */
    @ApiOperation(value = "上传图片",notes = "表单提交图片")
    @PostMapping(value = "/upload")
    @ResponseBody
    public RT imageUpload(MultipartFile imageFile) throws IOException {
        initDir();
        if(imageFile != null && !imageFile.isEmpty()){
            try {
                //生成随机ID
                String simpleUUID = IdUtil.simpleUUID();
                //将文件保存到指定目录
                String filePath = "/" + simpleUUID + ".jpg";
                imageFile.transferTo(new File(staticPath + filePath));
                return RT.ok("上传成功!",filePath);
            } catch (IOException e) {
                //e.printStackTrace();
                logger.error("图片上传失败：" + e.getMessage());
                return RT.error(RT.ERROR_0101,"图片上传失败!");
            }
        }else{
            return RT.error(RT.ERROR_0102,"没有选择上传文件!");
        }
    }

    /**
     * 下载图片
     * @param src 图片地址
     */
    @ApiOperation(value = "图片下载",notes = "根据地址下载图片")
    @GetMapping("/down")
    public void imageDown(String src, HttpServletResponse response) throws IOException {
        initDir();
        if(!"".equals(src)){
            File file  = new File(staticPath +src);
            if(file.exists()){
                try {
                    outputStream(response,file,file.getName());
                } catch (IOException e) {
                    //e.printStackTrace();
                    logger.error("图片下载失败：" + e.getMessage());
                }
            }
        }
    }

    /**
     * 输出文件流
     * @throws IOException 文件io异常
     */
    private void outputStream(HttpServletResponse response, File file,String fileName) throws IOException {
        InputStream in = new FileInputStream(file);
        //response.setContentType("image/jpeg");
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        response.addHeader("Content-Length", "" + file.length());
        OutputStream os = response.getOutputStream();
        byte[] b = new byte[1024];
        while( in.read(b)!= -1){
            os.write(b);
        }
        in.close();
        os.flush();
        os.close();
    }



    public void initDir() throws IOException {

        if(staticPath.equals("auto")){
            staticPath = FileUtil.file(".", "/upload").getCanonicalPath();
        }

        File file = new File(staticPath);
        //如果文件夹不存在则创建
        if  (!file .exists()  && !file .isDirectory())
        {
            boolean isMk = file .mkdirs();
            System.out.println("创建目录：" + staticPath + "  创建结果：" + isMk);
        }
    }
}

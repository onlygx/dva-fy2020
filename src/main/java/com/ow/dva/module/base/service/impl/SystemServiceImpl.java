package com.ow.dva.module.base.service.impl;

import com.ow.dva.module.base.service.SystemService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述 :
 * 包名 : com.ow.dva.module.base.service.impl
 * 作者 : GaoXiang
 * 时间 : 19-12-20
 * 版本 : V1.0
 */
@Service
public class SystemServiceImpl implements SystemService {

    @Override
    public Map<String,String> sysInfo() {
        Map<String,String> sysPro = new HashMap<>();
        sysPro.put("user.name",System.getProperties().getProperty("user.name"));
        sysPro.put("user.dir",System.getProperties().getProperty("user.dir"));
        sysPro.put("user.home",System.getProperties().getProperty("user.home"));
        sysPro.put("user.timezone",System.getProperties().getProperty("user.timezone"));
        sysPro.put("sun.boot.library.path",System.getProperties().getProperty("sun.boot.library.path"));
        sysPro.put("sun.java.command",System.getProperties().getProperty("sun.java.command"));
        sysPro.put("PID",System.getProperties().getProperty("PID"));
        sysPro.put("java.vm.name",System.getProperties().getProperty("java.vm.name"));
        sysPro.put("java.version",System.getProperties().getProperty("java.version"));
        sysPro.put("java.runtime.version",System.getProperties().getProperty("java.runtime.version"));
        sysPro.put("java.io.tmpdir",System.getProperties().getProperty("java.io.tmpdir"));
        sysPro.put("os.arch",System.getProperties().getProperty("os.arch"));
        sysPro.put("os.name",System.getProperties().getProperty("os.name"));
        sysPro.put("os.version",System.getProperties().getProperty("os.version"));
        sysPro.put("file.encoding",System.getProperties().getProperty("file.encoding"));
        sysPro.put("catalina.home",System.getProperties().getProperty("catalina.home"));
        sysPro.put("java.rmi.server.hostname",System.getProperties().getProperty("java.rmi.server.hostname"));
        return sysPro;
    }
}

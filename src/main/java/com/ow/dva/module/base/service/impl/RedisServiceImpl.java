package com.ow.dva.module.base.service.impl;

import com.ow.dva.module.base.service.RedisService;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * 描述 : 操作缓存
 * 包名 : com.ow.dva.module.base.service.impl
 *
 * @author GaoXiang
 * @version V1.1
 * @since 19-1-10
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Resource
    private CacheManager cacheManager;

    /**
     * 根据指定名称获取缓存
     * @param cacheName cache名称
     * @return 缓存操作对象
     */
    public Cache selectCache(String cacheName){
        return cacheManager.getCache(cacheName);
    }

    /**
     * 获取全部缓存名称
     * @return 缓存名称
     */
    public Collection<String> listCacheNames(){
        return cacheManager.getCacheNames();
    }

    /**
     * 清空缓存
     * @param cacheName 缓存名称
     */
    public void clear(String cacheName){
        Cache cache = selectCache(cacheName);
        cache.clear();
    }

    /**
     * 加入缓存
     * @param cacheName 缓存名称
     * @param key key
     * @param value value
     */
    public void put(String cacheName,Object key,Object value){
        Cache cache = selectCache(cacheName);
        cache.put(key,value);
    }

    /**
     * 清除指定缓存上的某个key
     * @param cacheName 缓存
     * @param key key
     */
    public void delete(String cacheName,Object key){
        Cache cache = selectCache(cacheName);
        cache.evict(key);
    }

    /**
     * 获取String类型的缓存
     * @param cacheName 缓存名称
     * @param key key
     * @return 缓存值
     */
    public String getString(String cacheName,String key){
        Cache cache = selectCache(cacheName);
        return cache.get(key,String.class);
    }

    /**
     * 获取自定义类型的缓存
     * @param cacheName 缓存名称
     * @param key key
     * @param clazz 缓存类型
     * @param <T> 类型
     * @return 缓存
     */
    public <T> T getObject(String cacheName,String key,Class<T> clazz){
        Cache cache = selectCache(cacheName);
        return cache.get(key,clazz);
    }
}

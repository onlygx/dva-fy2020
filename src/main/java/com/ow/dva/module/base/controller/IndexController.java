package com.ow.dva.module.base.controller;

import com.ow.dva.config.constant.UserConstant;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.account.entity.AccountAdmin;
import com.ow.dva.module.base.service.SystemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import javax.annotation.Resource;

/**
 * 描述 :
 * 包名 : com.ow.dva.module.base.controller
 * 作者 : GaoXiang
 * 时间 : 19-12-18
 * 版本 : V1.0
 */
@ApiIgnore
@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String index(@SessionAttribute(required = false) Account account,Model model){
        if(account != null){
            model.addAttribute(UserConstant.ACCOUNT,account);
            return "index";
        }
        return null;
    }

    @RequestMapping(value = "/home",method = RequestMethod.GET)
    public String home(@SessionAttribute(required = false) AccountAdmin accountAdmin,Model model){
        if(accountAdmin != null){
            model.addAttribute("sysInfo",systemService.sysInfo());
            return accountAdmin.getIndexPath();
        }
        model.addAttribute("sysInfo",systemService.sysInfo());
        return "body/home";
    }

    @Resource
    private SystemService systemService;

}

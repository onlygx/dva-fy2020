package com.ow.dva.module.base.entity.json;

import com.ow.dva.module.base.entity.base.BaseEntity;
import com.ow.dva.module.base.entity.param.DvaPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Collection;

/**
 * <p style="color:red;">
 *  消息返回类
 * </p>
 * @author GaoXiang
 * @since 2018-11-19
 * @version 1.1
 */
@ApiModel("Json通用返回类")
public class RT extends BaseEntity{

    /**
     * 默认成功代码
     */
    private final static Integer DEFAULT_SUCCESS_CODE = 200;                // 默认成功代码
    public final static Integer NO_RESOURCES_CODE = 404;                    // 没有资源代码
    public final static Integer DEFAULT_NOT_LOGIN = -1;                     // 会话异常代码
    public final static Integer DEFAULT_NOT_PERMISSION = -2;                // 权限异常代码
    public final static Integer DEFAULT_ERROR_CODE = 500;                   // 默认异常代码
    public final static Integer ERROR_0101 = 101;                           // 默认异常101
    public final static Integer ERROR_0102 = 102;                           // 默认异常102
    public final static Integer ERROR_0103 = 103;                           // 默认异常103
    public final static Integer ERROR_0104 = 104;                           // 默认异常104
    public final static Integer ERROR_0105 = 105;                           // 默认异常105
    public final static Integer NO_PERMISSION_ERROR_CODE = 120;             // 默认无权代码

    private final static String DEFAULT_SUCCESS_MSG = "操作成功";
    private final static String DEFAULT_ERROR_MSG = "操作成功";

    /**
     * 成功状态
     */
    @ApiModelProperty("请求结果状态，'true'：表示成功，'false'：表示失败。")
    private Boolean success;    // 成功状态

    @ApiModelProperty("请求结果代码，-1：表示无权限，0~99：表示状态代码，详见接口文档。")
    private Integer code;       // 状态代码

    @ApiModelProperty("请求结果描述，一般对应着'code'进行相关说明。")
    private String message;     // 返回消息

    @ApiModelProperty("请求结果数据，在列表查询中多用于参数回传，可能是任何类型，详见接口文档。")
    private Object data;        // 返回数据

    @ApiModelProperty("请求结果数据列表,带分页，请求返回的列表放在这里，详见接口文档。")
    private DvaPage page;

    @ApiModelProperty("请求结果数据列表，请求返回的列表放在这里，详见接口文档。")
    private Collection list;

    /**
     * 默认表示成功的构造方法
     */
    private RT(){
        this.success = true;
        this.code = DEFAULT_SUCCESS_CODE;
        this.message = DEFAULT_SUCCESS_MSG;
    }

    /**
     * 默认表示失败加错误代码和消息还有数据的构造方法
     *
     * @param code 代码
     * @param message  消息
     * @param data 数据
     */
    private RT(Integer code, String message, Object data){
        this.success = false;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 默认表示成功加消息和数据的构造方法
     *
     * @param message  消息
     * @param data 数据
     */
    private RT(String message, Object data){
        this.success = true;
        this.code = DEFAULT_SUCCESS_CODE;
        this.message = message;
        this.data = data;
    }


    /**
     * 默认表示成功加列表和参数的构造方法
     *
     * @param page  列表
     * @param param 参数
     */
    private RT(DvaPage page, Object param){
        this.success = true;
        this.code = DEFAULT_SUCCESS_CODE;
        this.message = DEFAULT_SUCCESS_MSG;
        this.page = page;
        this.list = page.getRecords();
        this.data = param;
    }

    /**
     * 默认表示成功加列表和参数的构造方法
     *
     * @param list  列表
     * @param param 参数
     */
    private RT(Collection list, Object param){
        this.success = true;
        this.code = DEFAULT_SUCCESS_CODE;
        this.message = DEFAULT_SUCCESS_MSG;
        this.list = list;
        this.data = param;
    }

    // ===============================  static  ===============================

    /**
     * 表示操作成功,返回默认成功状态.
     *
     * @return      返回包含以上字段的通知实体
     */
    public static RT ok(){
        return new RT();
    }

    /**
     * 表示成功并可返回一条消息在[message]字段
     *
     * @param msg   消息
     * @return      返回包含以上字段的通知实体
     */
    public static RT ok(String msg){
        return new RT(msg,null);
    }


    /**
     * 表示成功并可返回一段数据在[data]字段
     *
     * @param data  数据
     * @return      返回包含以上字段的通知实体
     */
    public static RT ok(Object data){
        return new RT(DEFAULT_SUCCESS_MSG,data);
    }

    /**
     * 表示成功并可返回一条消息在[message]字段和一段数据在[data]字段
     *
     * @param msg   消息
     * @param data  数据
     * @return      返回包含以上字段的通知实体
     */
    public static RT ok(String msg,Object data){
        return new RT(msg,data);
    }

    /**
     * 表示失败并返回一个错误代码在[code]字段
     *
     * @param code  错误代码
     * @return      返回包含以上字段的通知实体
     */
    public static RT error(Integer code){
        return new RT(code,DEFAULT_ERROR_MSG,null);
    }

    /**
     * 表示失败并返回一个错误代码在[code]字段和一条消息在[message]字段
     *
     * @param code  错误代码
     * @param msg   消息
     * @return      返回包含以上字段的通知实体
     */
    public static RT error(Integer code,String msg){
        return new RT(code,msg,null);
    }

    /**
     * 表示失败并返回一个错误代码在[code]字段和一条消息在[message]字段外加一段数据在[data]字段
     *
     * @param code  错误代码
     * @param msg   消息
     * @param data  数据
     * @return      返回包含以上字段的通知实体
     */
    public static RT error(Integer code,String msg,Object data){
        return new RT(code,msg,data);
    }

    public static RT list(DvaPage page, Object param) {
        return new RT(page,param);
    }

    public static RT list(Collection list, Object param) {
        return new RT(list,param);
    }

    public static RT status(boolean status,Object data) {
        if(status){
            return new RT("操作成功！",data);
        }else{
            return new RT(1,"操作失败！",data);
        }
    }

    // ================================  Get and Set  ================================

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public DvaPage getPage() {
        return page;
    }

    public void setPage(DvaPage page) {
        this.page = page;
    }

    public Collection getList() {
        return list;
    }

    public void setList(Collection list) {
        this.list = list;
    }
}

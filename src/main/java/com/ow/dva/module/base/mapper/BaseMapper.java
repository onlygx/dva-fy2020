package com.ow.dva.module.base.mapper;

/**
 * 描述 :
 * 包名 : com.ow.dva.module.base.mapper
 * 作者 : 高祥
 * 时间 : 2020/1/7
 * 版本 : V1.0
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T> {

}

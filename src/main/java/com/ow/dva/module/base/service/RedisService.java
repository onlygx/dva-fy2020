package com.ow.dva.module.base.service;

import org.springframework.cache.Cache;

import java.util.Collection;

/**
 * 描述 : 缓存操作类
 * 包名 : com.ow.dva.module.base.service
 * 作者 : GaoXiang
 * 时间 : 19-12-17
 * 版本 : V1.0
 */
public interface RedisService {

    /**
     * 获取某个缓存
     * @param cacheName 缓存名称
     * @return 缓存对象
     */
    Cache selectCache(String cacheName);

    /**
     * 获取全部缓存名称列表
     * @return 全部缓存
     */
    Collection<String> listCacheNames();

    /**
     * 清理某缓存
     * @param cacheName 缓存名称
     */
    void clear(String cacheName);

    /**
     * 存入缓存信息
     * @param cacheName 缓存名称
     * @param key 缓存key
     * @param value 缓存值
     */
    void put(String cacheName,Object key,Object value);

    /**
     * 删除某缓存中的指定key对应的数据
     * @param cacheName 缓存名称
     * @param key 缓存key
     */
    void delete(String cacheName,Object key);

    /**
     * 获取某缓存中指定key对应的内容
     * @param cacheName 缓存名称
     * @param key 缓存key
     * @return 缓存内容
     */
    String getString(String cacheName,String key);

    /**
     * 获取某缓存中指定key对应的内容 并转为java对象
     * @param cacheName 缓存
     * @param key 缓存key
     * @param clazz 转换类型
     * @param <T> 返回指定转换类型的数据
     * @return 返回数据转换成的对象
     */
    <T> T getObject(String cacheName,String key,Class<T> clazz);
}

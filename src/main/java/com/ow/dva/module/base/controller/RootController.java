package com.ow.dva.module.base.controller;


import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.base.service.SystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * 根环境访问路径
 */
@RestController
@Api(tags = "系统相关")
public class RootController extends BaseController {


    /**
     * 首页
     * @return 项目首页
     */
    @RequestMapping(value = "/path",method = RequestMethod.GET)
    @ApiOperation(value = "根目录path",notes = "默认地址")
    public RT path() {
        File path = null;
        try {
            path = new File(ResourceUtils.getURL("classpath:").getPath());
            return RT.ok("位置：" + path.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return RT.ok("位置获取失败。");
    }


    /**
     * 项目系统信息
     * @return 项目系统信息
     */
    @RequestMapping(value = "/sys/info",method = RequestMethod.GET)
    @ApiOperation(value = "系统信息",notes = "查看虚拟机/项目/系统信息")
    public RT sysInfo(){
        return RT.ok("获取成功!",systemService.sysInfo());
    }

    @Resource
    private SystemService systemService;
}

package com.ow.dva.module.base.entity.base;


import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * 公共Model切点类
 * @author GaoXiang
 * @since 2018/11/05
 * @version 1.0
 */
public class BaseEntity<T extends Model<?>> extends Model<T> {


}

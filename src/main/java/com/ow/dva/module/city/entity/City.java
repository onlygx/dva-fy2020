package com.ow.dva.module.city.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 地区信息
 * </p>
 *
 * @author 张仕济
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_city")
@ApiModel(value="City对象", description="地区信息")
public class City extends BaseEntity<City> {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty(value = "父id")
    private String parentId;

    @ApiModelProperty(value = "省市地区名称")
    private String name;

    @ApiModelProperty(value = "省市地区拼音")
    private String py;

    @ApiModelProperty(value = "邮政编码")
    private String code;

    @ApiModelProperty(value = "类型(0:国家;1:省份;2:城市;3:地区)")
    private Integer type;

    @ApiModelProperty(value = "直辖市的标记(0:否;1:是)")
    private Integer special;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPy() {
        return py;
    }

    public void setPy(String py) {
        this.py = py;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Integer getSpecial() {
        return special;
    }

    public void setSpecial(Integer special) {
        this.special = special;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "City{" +
            "id=" + id +
            ", parentId=" + parentId +
            ", name=" + name +
            ", py=" + py +
            ", code=" + code +
            ", type=" + type +
            ", special=" + special +
        "}";
    }
}

package com.ow.dva.module.city.service.impl;

import com.ow.dva.module.city.entity.City;
import com.ow.dva.module.city.mapper.CityMapper;
import com.ow.dva.module.city.service.CityService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地区信息 服务实现类
 * </p>
 *
 * @author 张仕济
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class CityServiceImpl extends DvaServiceImpl<CityMapper, City> implements CityService {

    @DvaCache5M
    @Override
    public DvaPage<City> page(DvaPage<City> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<City> findChild(String cityId) {
        return baseMapper.findChild(cityId);
    }

    @Override
    public List<City> selectProvince() {
        return baseMapper.selectProvince();
    }
}

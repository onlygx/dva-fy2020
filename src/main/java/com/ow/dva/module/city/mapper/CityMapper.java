package com.ow.dva.module.city.mapper;

import com.ow.dva.module.city.entity.City;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地区信息 Mapper 接口
 * </p>
 *
 * @author 张仕济
 * @since 2020-02-15
 * @version V1.0
 */
public interface CityMapper extends BaseMapper<City> {

    /**
    * 分页获取地区信息数据列表信息。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<City> page(DvaPage<City> page, @Param("param") Map<String, Object> param);

    List<City> findChild(String cityId);

    List<City> selectProvince();
}

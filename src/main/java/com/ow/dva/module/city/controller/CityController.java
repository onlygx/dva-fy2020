package com.ow.dva.module.city.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.city.entity.City;
import com.ow.dva.module.city.service.CityService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 地区信息 前端控制器
 * </p>
 *
 * @author 张仕济
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "地区信息 相关控制器")
@RequestMapping("/city")
public class CityController extends BaseController {

    /**
    * 分页获取地区信息数据列表信息
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取地区信息信息列表，分页，可携带条件")
    @RequiresPermissions("city:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<City> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<City> page = cityService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "city/list";
    }

    /**
    * 进入地区信息添加页面。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入地区信息添加页面")
    @RequiresPermissions("city:add")
    @GetMapping(value = "/add")
    public String add(){

        return "city/add";
    }

    /**
    * 添加地区信息信息。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个地区信息信息")
    @RequiresPermissions("city:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(City city){

        return cityService.save(city) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入地区信息编辑页面。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入地区信息编辑页面")
    @RequiresPermissions("city:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",cityService.getById(id));

        return "city/edit";
    }

    /**
    * 修改地区信息信息。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个地区信息信息")
    @RequiresPermissions("city:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(City city){

        return cityService.updateById(city) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个地区信息信息。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个地区信息信息")
    @RequiresPermissions("city:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的地区信息ID") String id){

        return cityService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个地区信息信息。
    * 创建时间：2020-02-15 张仕济
    * 修改时间：2020-02-15 张仕济 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个地区信息信息")
    @RequiresPermissions("city:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的地区信息ID List") @RequestParam List<String> ids){

        return cityService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    /**
     * 根据地区id查询下级地区。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "根据地区id查询下级地区",notes = "查询多个地区信息信息")
//    @RequiresPermissions("city:findChild")
    @PostMapping("/findChild")
    @ResponseBody
    public RT findChild(String cityId){

        List<City> list = cityService.findChild(cityId);
        return list.size() > 0 ? RT.ok(list) : RT.error(RT.ERROR_0101,"暂无数据");
    }


    /**
     * 查询省级地区。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "查询省级地区",notes = "查询省级地区")
    @RequiresPermissions("city:selectProvince")
    @PostMapping("/selectProvince")
    @ResponseBody
    public RT selectProvince(){

        List<City> list = cityService.selectProvince();
        return list.size() > 0 ? RT.ok(list) : RT.error(RT.ERROR_0101,"暂无数据");
    }
    @Resource
    CityService cityService;
}

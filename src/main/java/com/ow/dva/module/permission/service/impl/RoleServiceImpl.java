package com.ow.dva.module.permission.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.permission.entity.Role;
import com.ow.dva.module.permission.mapper.RoleMapper;
import com.ow.dva.module.permission.service.RoleService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class RoleServiceImpl extends DvaServiceImpl<RoleMapper, Role> implements RoleService {

    @DvaCache5M
    @Override
    public DvaPage<Role> page(DvaPage<Role> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<Role> selectByAccountId(String accountId) {

        return baseMapper.selectByAccountId(accountId);
    }

    @Override
    public List<Role> selectAllWithAccountId(String accountId) {

        return baseMapper.selectAllWithAccountId(accountId);
    }
}

package com.ow.dva.module.permission.mapper;

import com.ow.dva.module.permission.entity.AccountRole;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;
import java.util.Map;

/**
 * <p>
 * 用户 <->角色 Mapper 接口
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface AccountRoleMapper extends BaseMapper<AccountRole> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<AccountRole> page(DvaPage<AccountRole> page, @Param("param") Map<String, Object> param);

    /**
     * 删除全部关于指定 authorityId 的数据
     * 创建时间：2020-01-13 高祥
     * 修改时间：2020-01-13 高祥 加注释
     * 修改时间：
     * @param accountId 用户ID
     * @return 更改的行数
     */
    int deleteByAccountId(String accountId);

    /**
     * 删除全部关于指定 roleId 的数据
     * 创建时间：2020-01-13 高祥
     * 修改时间：2020-01-13 高祥 加注释
     * 修改时间：
     * @param roleId 用户ID
     * @return 更改的行数
     */
    int deleteByRoleId(String roleId);
}

package com.ow.dva.module.permission.controller;


import com.ow.dva.module.permission.entity.RolePermission;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.permission.service.RolePermissionService;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 角色 <-> 权限 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Controller
@Api(tags = "角色 <-> 权限 相关控制器")
@RequestMapping("/rolePermission")
public class RolePermissionController extends BaseController {

    /**
    * 分页获取角色 <-> 权限数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取角色 <-> 权限信息列表，分页，可携带条件")
    @GetMapping(value = "/page")
    public String page(DvaPage<RolePermission> dvaPage, @RequestParam Map<String, Object> param, Model model){

        model.addAttribute("page",rolePermissionService.page(dvaPage,param));

        return "rolePermission/list";
    }

    /**
    * 进入角色 <-> 权限添加页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入角色 <-> 权限添加页面")
    @GetMapping(value = "/add")
    public String add(){

        return "rolePermission/add";
    }

    /**
    * 添加角色 <-> 权限信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个角色 <-> 权限信息")
    @PostMapping("/save")
    @ResponseBody
    public RT save(RolePermission rolePermission){

        return rolePermissionService.save(rolePermission) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入角色 <-> 权限编辑页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入角色 <-> 权限编辑页面")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",rolePermissionService.getById(id));

        return "rolePermission/edit";
    }

    /**
    * 修改角色 <-> 权限信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个角色 <-> 权限信息")
    @PostMapping("/update")
    @ResponseBody
    public RT update(RolePermission rolePermission){

        return rolePermissionService.updateById(rolePermission) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个角色 <-> 权限信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个角色 <-> 权限信息")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的角色 <-> 权限ID") String id){

        return rolePermissionService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个角色 <-> 权限信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个角色 <-> 权限信息")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的角色 <-> 权限ID List") @RequestParam List<String> ids){

        return rolePermissionService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    RolePermissionService rolePermissionService;
}

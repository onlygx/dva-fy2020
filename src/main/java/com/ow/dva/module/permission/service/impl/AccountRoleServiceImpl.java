package com.ow.dva.module.permission.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.permission.entity.AccountRole;
import com.ow.dva.module.permission.mapper.AccountRoleMapper;
import com.ow.dva.module.permission.service.AccountRoleService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 用户 <->角色 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class AccountRoleServiceImpl extends DvaServiceImpl<AccountRoleMapper, AccountRole> implements AccountRoleService {

    @DvaCache5M
    @Override
    public DvaPage<AccountRole> page(DvaPage<AccountRole> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public int deleteByAccountId(String accountId) {

        return baseMapper.deleteByAccountId(accountId);
    }

    @Override
    public int deleteByRoleId(String roleId) {

        return baseMapper.deleteByRoleId(roleId);
    }
}

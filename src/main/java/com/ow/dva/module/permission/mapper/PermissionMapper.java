package com.ow.dva.module.permission.mapper;

import com.ow.dva.module.permission.entity.Permission;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Permission> page(DvaPage<Permission> page, @Param("param") Map<String, Object> param);

    /**
     * 根据用户ID获取用户所有权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param accountId 用户ID
     * @return 权限列表
     */
    List<Permission> selectByAccountId(String accountId);

    /**
     * 角色授权权限时专用
     * 获取全部权限，指定角色做过关联的拥有roleId字段，没有的该字段为空
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<Permission> selectAllWithRoleId(String roleId);

    /**
     * 获取指定角色下全部权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<Permission> selectByRoleId(String roleId);

    /**
     * 获取指定父级权限下全部权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param parentId 父级权限ID
     * @return 权限列表
     */
    List<Permission> selectByParentId(String parentId);
}

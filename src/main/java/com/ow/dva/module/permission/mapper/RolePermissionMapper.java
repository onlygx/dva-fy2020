package com.ow.dva.module.permission.mapper;

import com.ow.dva.module.permission.entity.RolePermission;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;
import java.util.Map;

/**
 * <p>
 * 角色 <-> 权限 Mapper 接口
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<RolePermission> page(DvaPage<RolePermission> page, @Param("param") Map<String, Object> param);

    /**
     * 删除全部关于指定RoleId的数据
     * 创建时间：2020-01-13 高祥
     * 修改时间：2020-01-13 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     * @return 更改的行数
     */
    int deleteByRoleId(String roleId);

    /**
     * 删除全部关于指定 authorityId 的数据
     * 创建时间：2020-01-13 高祥
     * 修改时间：2020-01-13 高祥 加注释
     * 修改时间：
     * @param authorityId 权限ID
     * @return 更改的行数
     */
    int deleteByAuthorityId(String authorityId);

}

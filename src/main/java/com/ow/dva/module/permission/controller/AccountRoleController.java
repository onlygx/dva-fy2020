package com.ow.dva.module.permission.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.permission.entity.AccountRole;
import com.ow.dva.module.permission.service.AccountRoleService;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户 <->角色 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Controller
@Api(tags = "用户 <->角色 相关控制器")
@RequestMapping("/accountRole")
public class AccountRoleController extends BaseController {

    /**
    * 分页获取用户 <->角色数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取用户 <->角色信息列表，分页，可携带条件")
    @GetMapping(value = "/page")
    public String page(DvaPage<AccountRole> dvaPage, @RequestParam Map<String, Object> param, Model model){

        model.addAttribute("page",accountRoleService.page(dvaPage,param));

        return "accountRole/list";
    }

    /**
    * 进入用户 <->角色添加页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入用户 <->角色添加页面")
    @GetMapping(value = "/add")
    public String add(){

        return "accountRole/add";
    }

    /**
    * 添加用户 <->角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个用户 <->角色信息")
    @PostMapping("/save")
    @ResponseBody
    public RT save(AccountRole accountRole){

        return accountRoleService.save(accountRole) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入用户 <->角色编辑页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入用户 <->角色编辑页面")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",accountRoleService.getById(id));

        return "accountRole/edit";
    }

    /**
    * 修改用户 <->角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个用户 <->角色信息")
    @PostMapping("/update")
    @ResponseBody
    public RT update(AccountRole accountRole){

        return accountRoleService.updateById(accountRole) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个用户 <->角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个用户 <->角色信息")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的用户 <->角色ID") String id){

        return accountRoleService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个用户 <->角色信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个用户 <->角色信息")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的用户 <->角色ID List") @RequestParam List<String> ids){

        return accountRoleService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    AccountRoleService accountRoleService;
}

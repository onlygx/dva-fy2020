package com.ow.dva.module.permission.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.permission.entity.RolePermission;
import com.ow.dva.module.permission.mapper.RolePermissionMapper;
import com.ow.dva.module.permission.service.RolePermissionService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 角色 <-> 权限 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class RolePermissionServiceImpl extends DvaServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    @DvaCache5M
    @Override
    public DvaPage<RolePermission> page(DvaPage<RolePermission> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public int deleteByRoleId(String roleId) {

        return baseMapper.deleteByRoleId(roleId);
    }

    @Override
    public int deleteByAuthorityId(String authorityId) {

        return baseMapper.deleteByAuthorityId(authorityId);
    }
}

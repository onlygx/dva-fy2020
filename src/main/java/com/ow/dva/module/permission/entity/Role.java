package com.ow.dva.module.permission.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@TableName("dva_role")
@ApiModel(value="Role对象", description="用户角色")
public class Role extends BaseEntity<Role> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "角色简介")
    private String intro;

    @ApiModelProperty(value = "角色标识")
    private String shiro;

    @ApiModelProperty(hidden = true)
    @TableField(exist = false)
    private String accountId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getShiro() {
        return shiro;
    }

    public void setShiro(String shiro) {
        this.shiro = shiro;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Role{" +
            "id=" + id +
            ", name=" + name +
            ", intro=" + intro +
            ", shiro=" + shiro +
        "}";
    }
}

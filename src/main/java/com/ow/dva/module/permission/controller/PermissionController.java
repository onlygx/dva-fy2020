package com.ow.dva.module.permission.controller;


import com.ow.dva.module.permission.entity.Permission;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.permission.service.PermissionService;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Controller
@Api(tags = "权限表 相关控制器")
@RequestMapping("/permission")
public class PermissionController extends BaseController {

    /**
     * 根据roleID获取所有权限，并且显示那些属于这个角色，带格式
     * 用于角色授权
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     */
    @GetMapping("/findByRoleId")
    public String findByRoleId(String roleId, ModelMap model){
        List<Permission> list = permissionService.selectAllWithRoleId(roleId);
        model.addAttribute("permissionList",list);
        return "permission/choose";
    }

    /**
     * 获取全部权限 - 带格式
     * 用于权限选择上级
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     */
    @GetMapping("/listAll")
    public String chooseParent(ModelMap model){
        List<Permission> list = permissionService.listAll();
        model.addAttribute("permissionList",list);
        return "permission/chooseParent";
    }

    /**
     * 统计这一个父类下的权限有多少个，用于判断该权限排序
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     * @param parentId 父级权限ID
     */
    @PostMapping("/countByParentId")
    @ResponseBody
    public Integer countByParentId(String parentId){
        List<Permission> list = permissionService.selectByParentId(parentId);
        return list.size();
    }

    /**
    * 分页获取权限表数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取权限表信息列表，分页，可携带条件")
    @GetMapping(value = "/page")
    public String page(DvaPage<Permission> dvaPage, @RequestParam Map<String, Object> param, Model model){

        model.addAttribute("page", permissionService.page(dvaPage,param));
        model.addAttribute("param", param);
        return "permission/list";
    }

    /**
    * 进入权限表添加页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入权限表添加页面")
    @GetMapping(value = "/add")
    public String add(){

        return "permission/add";
    }

    /**
     * 进入权限表快速添加页面。
     * 创建时间：2020-02-14 高祥
     * 修改时间：2020-02-14 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入快速添加页面",notes = "进入权限表快速添加页面")
    @GetMapping(value = "/quickAdd")
    public String quickAdd(){

        return "permission/quickAdd";
    }

    /**
    * 添加权限表信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个权限表信息")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Permission permission){

        return permissionService.save(permission) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
     * 一次性添加权限表信息。添加增删改查以及列表管理
     * 创建时间：2020-01-09 高祥
     * 修改时间：2020-01-09 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "添加",notes = "添加单个权限表信息")
    @PostMapping("/quickSave")
    @ResponseBody
    public RT quickSave(String name,String url){
        return permissionService.quickSave(name,url) ? RT.ok() : RT.error(RT.ERROR_0101);
    }


    /**
    * 进入权限表编辑页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入权限表编辑页面")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data", permissionService.getById(id));

        return "permission/edit";
    }

    /**
    * 修改权限表信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个权限表信息")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Permission permission){

        return permissionService.updateById(permission) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个权限表信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个权限表信息")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的权限表ID") String id){

        return permissionService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个权限表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个权限表信息")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的权限表ID List") @RequestParam List<String> ids){

        return permissionService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    PermissionService permissionService;
}

package com.ow.dva.module.permission.service;

import com.ow.dva.module.permission.entity.Permission;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface PermissionService extends DvaService<Permission> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Permission> page(DvaPage<Permission> page, Map<String,Object> param);

    /**
     * 获取全部权限，主要用于权限父类选择
     * 创建时间：2020-02-12 高祥
     * 修改时间：2020-02-12 高祥 加注释
     * 修改时间：
     * @return 全部权限，带格式
     */
    List<Permission> listAll();

    /**
     * 根据用户ID获取用户所有权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param accountId 用户ID
     * @return 权限列表
     */
    List<Permission> selectByAccountId(String accountId);

    /**
     * 角色授权时专用
     * 获取全部权限，指定角色做过关联的拥有roleId字段，没有的该字段为空
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<Permission> selectAllWithRoleId(String roleId);

    /**
     * 获取指定角色下全部权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param roleId 角色ID
     * @return 权限列表
     */
    List<Permission> selectByRoleId(String roleId);

    /**
     * 获取指定父级权限下全部权限
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param parentId 父级权限ID
     * @return 权限列表
     */
    List<Permission> selectByParentId(String parentId);

    /**
     * 快速生成增删改查
     * 创建时间：2020-02-12 高祥
     * 修改时间：2020-02-12 高祥 加注释
     * 修改时间：
     * @param name 权限名称 中文
     * @param url 权限地址 英文
     * @return 权限列表
     */
    boolean quickSave(String name, String url);
}

package com.ow.dva.module.permission.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户 <->角色
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@TableName("dva_account_role")
@ApiModel(value="AccountRole对象", description="用户 <->角色")
public class AccountRole extends BaseEntity<AccountRole> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "用户ID")
    private String accountId;

    @ApiModelProperty(value = "角色ID")
    private String roleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AccountRole{" +
            "id=" + id +
            ", accountId=" + accountId +
            ", roleId=" + roleId +
        "}";
    }
}

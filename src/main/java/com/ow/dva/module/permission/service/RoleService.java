package com.ow.dva.module.permission.service;

import com.ow.dva.module.permission.entity.Role;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface RoleService extends DvaService<Role> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Role> page(DvaPage<Role> page, Map<String,Object> param);

    /**
     * 根据用户Id获取用户下所有角色
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param accountId 用户ID
     * @return 角色列表
     */
    List<Role> selectByAccountId(String accountId);

    /**
     * 用户授权角色时专用
     * 获取所有角色，属于指定用户的角色会携带用户Id
     * 创建时间：2020-01-12 高祥
     * 修改时间：2020-01-12 高祥 加注释
     * 修改时间：
     * @param accountId 用户ID
     * @return 角色列表
     */
    List<Role> selectAllWithAccountId(String accountId);
}

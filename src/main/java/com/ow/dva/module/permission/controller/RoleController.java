package com.ow.dva.module.permission.controller;


import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.permission.entity.Role;
import com.ow.dva.module.permission.service.RoleService;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户角色 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Controller
@Api(tags = "用户角色 相关控制器")
@RequestMapping("/role")
public class RoleController extends BaseController {


    /**
     * 根据roleID获取所有权限，并且显示那些属于这个角色
     * 用于角色授权
     * @param accountId  账号ID
     * @return 页面
     */
    @GetMapping("/findByAccountInfo")
    public String findByAccountInfo(String accountId, ModelMap model){
        List<Role> roleList = roleService.selectAllWithAccountId(accountId);
        model.addAttribute("roleList",roleList);
        return "role/choose";
    }

    /**
    * 分页获取用户角色数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取用户角色信息列表，分页，可携带条件")
    @GetMapping(value = "/page")
    public String page(DvaPage<Role> dvaPage, @RequestParam Map<String, Object> param, Model model){

        model.addAttribute("page",roleService.page(dvaPage,param));

        return "role/list";
    }

    /**
    * 进入用户角色添加页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入用户角色添加页面")
    @GetMapping(value = "/add")
    public String add(){

        return "role/add";
    }

    /**
    * 添加用户角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个用户角色信息")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Role role){

        return roleService.save(role) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入用户角色编辑页面。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入用户角色编辑页面")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",roleService.getById(id));

        return "role/edit";
    }

    /**
    * 修改用户角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个用户角色信息")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Role role){

        return roleService.updateById(role) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个用户角色信息。
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个用户角色信息")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的用户角色ID") String id){

        return roleService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个用户角色信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个用户角色信息")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的用户角色ID List") @RequestParam List<String> ids){

        return roleService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    RoleService roleService;
}

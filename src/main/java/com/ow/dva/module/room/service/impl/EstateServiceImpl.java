package com.ow.dva.module.room.service.impl;

import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.mapper.EstateMapper;
import com.ow.dva.module.room.service.EstateService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 小区 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class EstateServiceImpl extends DvaServiceImpl<EstateMapper, Estate> implements EstateService {

    @DvaCache5M
    @Override
    public DvaPage<Estate> page(DvaPage<Estate> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<Estate> selectAll() {
        return baseMapper.selectAll();
    }

    @Override
    public Estate selectById(String estateId) {
        return baseMapper.selectById(estateId);
    }
}

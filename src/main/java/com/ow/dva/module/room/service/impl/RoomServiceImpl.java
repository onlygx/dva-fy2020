package com.ow.dva.module.room.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.people.mapper.CarMapper;
import com.ow.dva.module.people.mapper.PeopleMapper;
import com.ow.dva.module.people.mapper.PetMapper;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.room.mapper.RoomMapper;
import com.ow.dva.module.room.service.RoomService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 住房 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class RoomServiceImpl extends DvaServiceImpl<RoomMapper, Room> implements RoomService {


    @Resource
    PeopleMapper peopleMapper;
    @Resource
    CarMapper carMapper;
    @Resource
    PetMapper petMapper;


    @DvaCache5M
    @Override
    public DvaPage<Room> page(DvaPage<Room> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public boolean saveRoomInfo(Room room) {
        int flag = baseMapper.insert(room);
        if(flag > 0){
            //房屋下的人员添加
            savePeople(room);
            //房屋下的车辆添加
            saveCar(room);
            //房屋下的宠物添加
            savePet(room);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Room selectRoomInfoById(String roomId) {
        Room room = baseMapper.selectById(roomId);
        if(room != null){
            //查询人
            selectPeople(room);
            //查询车辆
            selectCar(room);
            //查询宠物
            selectPet(room);
        }
        return room;
    }

    private void selectPeople(Room room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","room");
        queryWrapper.eq("from_id",room.getId());
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        room.setPeopleList(peopleList);
    }

    private void selectCar(Room room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","room");
        queryWrapper.eq("from_id",room.getId());
        List<Car> carList = carMapper.selectList(queryWrapper);
        room.setCarList(carList);
    }

    private void selectPet(Room room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","room");
        queryWrapper.eq("from_id",room.getId());
        List<Pet> petList = petMapper.selectList(queryWrapper);
        room.setPetList(petList);
    }

    @Override
    public Room selectByNameAndBuilding(String buildingId, String name) {
        return baseMapper.selectByNameAndBuilding(buildingId,name);
    }

    @Override
    public Room selectById(String roomId) {
        return baseMapper.selectById(roomId);
    }

    //    房屋下的人员添加
    private void savePeople(Room room){
        List<People> peopleList = room.getPeopleList();
        if (peopleList != null && !peopleList.isEmpty()){
            for (People people: peopleList) {
                people.setFromType("room");
                people.setFromId(room.getId());
                people.setOrganizationId(room.getOrganizationId());
                peopleMapper.insert(people);
            }
        }
    }

//    房屋下的车辆添加
    private void saveCar(Room room){
        List<Car> carList = room.getCarList();
        if (carList != null && !carList.isEmpty()){
            for (Car car: carList) {
                car.setFromType("room");
                car.setFromId(room.getId());
                car.setOrganizationId(room.getOrganizationId());
                carMapper.insert(car);
            }
        }
    }

//    房屋下的宠物添加
    private void savePet(Room room){
        List<Pet> petList = room.getPetList();
        if (petList != null && !petList.isEmpty()){
            for (Pet pet: petList) {
                pet.setFromType("room");
                pet.setFromId(room.getId());
                pet.setOrganizationId(room.getOrganizationId());
                petMapper.insert(pet);
            }
        }
    }


}

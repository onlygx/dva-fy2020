package com.ow.dva.module.room.controller;


import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.service.EstateService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.room.entity.Building;
import com.ow.dva.module.room.service.BuildingService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 楼栋 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "楼栋 相关控制器")
@RequestMapping("/building")
public class BuildingController extends BaseController {

    /**
    * 分页获取楼栋数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取楼栋信息列表，分页，可携带条件")
    @RequiresPermissions("building:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Building> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Building> page = buildingService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "building/list";
    }

    /**
    * 进入楼栋添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入楼栋添加页面")
    @RequiresPermissions("building:add")
    @GetMapping(value = "/add")
    public String add(){

        return "building/add";
    }

    /**
    * 添加楼栋信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个楼栋信息")
    @RequiresPermissions("building:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Building building){
        Estate estate = estateService.selectById(building.getEstateId());
        building.setOrganizationId(estate.getOrganizationId());
        return buildingService.save(building) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入楼栋编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入楼栋编辑页面")
    @RequiresPermissions("building:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",buildingService.getById(id));

        return "building/edit";
    }

    /**
    * 修改楼栋信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个楼栋信息")
    @RequiresPermissions("building:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Building building){

        return buildingService.updateById(building) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个楼栋信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个楼栋信息")
    @RequiresPermissions("building:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的楼栋ID") String id){

        return buildingService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个楼栋信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个楼栋信息")
    @RequiresPermissions("building:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的楼栋ID List") @RequestParam List<String> ids){

        return buildingService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }

    /**
     * 创建楼栋。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入编辑页面",notes = "进入楼栋编辑页面")
    @RequiresPermissions("building:create")
    @GetMapping(value = "/create")
    public String edit(Model model){
        List<Estate> list = estateService.selectAll();
        model.addAttribute("list",list);
//        model.addAttribute("data",buildingService.getById(id));
        return "building/create";
    }

    /**
     * 根据小区查询小区楼栋
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入编辑页面",notes = "进入楼栋编辑页面")
    @PostMapping(value = "/findByEstateId")
    @ResponseBody
    public RT findByEstateId(String estateId){
        List<Building> list =  buildingService.selectByEstateId(estateId);
        return list.size() > 0 ? RT.ok(list) : RT.error(RT.ERROR_0101,"暂无数据");
    }


    @Resource
    BuildingService buildingService;
    @Resource
    EstateService estateService;
}

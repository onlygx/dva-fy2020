package com.ow.dva.module.room.service.impl;

import com.ow.dva.module.room.entity.Unit;
import com.ow.dva.module.room.mapper.UnitMapper;
import com.ow.dva.module.room.service.UnitService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单元 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class UnitServiceImpl extends DvaServiceImpl<UnitMapper, Unit> implements UnitService {

    @DvaCache5M
    @Override
    public DvaPage<Unit> page(DvaPage<Unit> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<Unit> selectByBuildingId(String buildingId) {
        return baseMapper.selectByBuildingId(buildingId);
    }
}

package com.ow.dva.module.room.controller;


import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.service.BuildingService;
import com.ow.dva.module.room.service.EstateService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.room.entity.Unit;
import com.ow.dva.module.room.service.UnitService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 单元 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "单元 相关控制器")
@RequestMapping("/unit")
public class UnitController extends BaseController {

    /**
    * 分页获取单元数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取单元信息列表，分页，可携带条件")
    @RequiresPermissions("unit:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Unit> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Unit> page = unitService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "unit/list";
    }

    /**
    * 进入单元添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入单元添加页面")
    @RequiresPermissions("unit:add")
    @GetMapping(value = "/add")
    public String add(Model model){
        List<Estate> estates = estateService.selectAll();
        model.addAttribute("estates",estates);
        return "unit/add";
    }

    /**
    * 添加单元信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个单元信息")
    @RequiresPermissions("unit:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Unit unit){
        Estate estate = estateService.selectById(unit.getEstateId());
        unit.setOrganizationId(estate.getOrganizationId());
        return unitService.save(unit) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入单元编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入单元编辑页面")
    @RequiresPermissions("unit:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",unitService.getById(id));

        return "unit/edit";
    }

    /**
    * 修改单元信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个单元信息")
    @RequiresPermissions("unit:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Unit unit){

        return unitService.updateById(unit) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个单元信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个单元信息")
    @RequiresPermissions("unit:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的单元ID") String id){

        return unitService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个单元信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个单元信息")
    @RequiresPermissions("unit:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的单元ID List") @RequestParam List<String> ids){

        return unitService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    /**
     * 根据楼栋获得单元信息
     * 创建时间：2020-02-15 高祥
     * 修改时间：2020-02-15 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "根据楼栋获得单元信息",notes = "根据楼栋获得单元信息")
    @PostMapping(value = "/findByBuildingId")
    @ResponseBody
    public RT findByBuildingId(String buildingId){
        List<Unit> list =  unitService.selectByBuildingId(buildingId);
        return list.size() > 0 ? RT.ok(list) : RT.error(RT.ERROR_0101,"暂无数据");
    }

    @Resource
    UnitService unitService;
    @Resource
    EstateService estateService;
    @Resource
    BuildingService buildingService;
}

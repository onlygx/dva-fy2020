package com.ow.dva.module.room.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 住房
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_room")
@ApiModel(value="Room对象", description="住房")
public class Room extends BaseEntity<Room> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "房间名称")
    private String name;

    @ApiModelProperty(value = "房屋地址")
    private String address;

    @ApiModelProperty(value = "房屋类型")
    private String type;

    @ApiModelProperty(value = "所属小区")
    private String estateId;

    @ApiModelProperty(value = "所属楼栋")
    private String buildingId;

    @ApiModelProperty(value = "所属单元")
    private String unitId;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @ApiModelProperty(value = "房屋性质")
    private String xingzhi;

    @ApiModelProperty(value = "房屋形式")
    private String xingshi;

    @ApiModelProperty(value = "室内面积")
    private String area;

    @ApiModelProperty(value = "房间数")
    private String roomCount;

    @ApiModelProperty(value = "房主姓名")
    private String ownerName;

    @ApiModelProperty(value = "房主身份证")
    private String ownerCard;

    @ApiModelProperty(value = "房主电话")
    private String ownerPhone;

    @ApiModelProperty(value = "房主户籍")
    private String ownerAddress;

    @ApiModelProperty(value = "房主实际居住地址")
    private String ownerRealAddress;

    @ApiModelProperty(value = "租赁状态")
    private String zulin;

    @ApiModelProperty(value = "治安责任书")
    private String zhian;

    @ApiModelProperty(value = "租金")
    private String zujin;

    @ApiModelProperty(value = "起租日期")
    private String qizuriqi;

    @ApiModelProperty(value = "租期")
    private String zuqi;

    @ApiModelProperty(value = "托管人姓名")
    private String tuoguanName;

    @ApiModelProperty(value = "托管人电话")
    private String tuoguanPhone;

    @ApiModelProperty(value = "是否有无人机")
    private String wurenji;

    @ApiModelProperty(value = "无人机型号")
    private String wurenjiXinghao;

    @ApiModelProperty(value = "无人机序列号")
    private String wurenjiXulie;

    @ApiModelProperty(value = "无人机飞手")
    private String wurenjiOwner;

    @ApiModelProperty(value = "是否有监控")
    private String jiankong;

    @ApiModelProperty(value = "监控数量")
    private String jiankongCount;

    @ApiModelProperty(value = "消防设施")
    private String xiaofang;

    @ApiModelProperty(value = "其它备注")
    private String intro;

    @ApiModelProperty(value = "采集类型")
    private String caijiType;

    @ApiModelProperty(value = "采集人员")
    private String caijiAccount;


    @ApiModelProperty(value = "登记日期")
    private Date createTime;

    /**
     * 自定义 人员集合
     */
    @TableField(exist = false)
    private List<People> peopleList;

    /**
     * 自定义 宠物集合
     */
    @TableField(exist = false)
    private List<Pet> petList;

    /**
     * 自定义 车辆集合
     */
    @TableField(exist = false)
    private List<Car> carList;

    @TableField(exist = false)
    private String images;

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOwnerRealAddress() {
        return ownerRealAddress;
    }

    public void setOwnerRealAddress(String ownerRealAddress) {
        this.ownerRealAddress = ownerRealAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEstateId() {
        return estateId;
    }

    public void setEstateId(String estateId) {
        this.estateId = estateId;
    }
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
    public String getXingzhi() {
        return xingzhi;
    }

    public void setXingzhi(String xingzhi) {
        this.xingzhi = xingzhi;
    }
    public String getXingshi() {
        return xingshi;
    }

    public void setXingshi(String xingshi) {
        this.xingshi = xingshi;
    }
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    public String getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(String roomCount) {
        this.roomCount = roomCount;
    }
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    public String getOwnerCard() {
        return ownerCard;
    }

    public void setOwnerCard(String ownerCard) {
        this.ownerCard = ownerCard;
    }
    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }
    public String getOwnerAddress() {
        return ownerAddress;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }
    public String getZulin() {
        return zulin;
    }

    public void setZulin(String zulin) {
        this.zulin = zulin;
    }
    public String getZhian() {
        return zhian;
    }

    public void setZhian(String zhian) {
        this.zhian = zhian;
    }
    public String getZujin() {
        return zujin;
    }

    public void setZujin(String zujin) {
        this.zujin = zujin;
    }
    public String getQizuriqi() {
        return qizuriqi;
    }

    public void setQizuriqi(String qizuriqi) {
        this.qizuriqi = qizuriqi;
    }
    public String getZuqi() {
        return zuqi;
    }

    public void setZuqi(String zuqi) {
        this.zuqi = zuqi;
    }
    public String getTuoguanName() {
        return tuoguanName;
    }

    public void setTuoguanName(String tuoguanName) {
        this.tuoguanName = tuoguanName;
    }
    public String getTuoguanPhone() {
        return tuoguanPhone;
    }

    public void setTuoguanPhone(String tuoguanPhone) {
        this.tuoguanPhone = tuoguanPhone;
    }
    public String getWurenji() {
        return wurenji;
    }

    public void setWurenji(String wurenji) {
        this.wurenji = wurenji;
    }
    public String getWurenjiXinghao() {
        return wurenjiXinghao;
    }

    public void setWurenjiXinghao(String wurenjiXinghao) {
        this.wurenjiXinghao = wurenjiXinghao;
    }
    public String getWurenjiXulie() {
        return wurenjiXulie;
    }

    public void setWurenjiXulie(String wurenjiXulie) {
        this.wurenjiXulie = wurenjiXulie;
    }
    public String getWurenjiOwner() {
        return wurenjiOwner;
    }

    public void setWurenjiOwner(String wurenjiOwner) {
        this.wurenjiOwner = wurenjiOwner;
    }
    public String getJiankong() {
        return jiankong;
    }

    public void setJiankong(String jiankong) {
        this.jiankong = jiankong;
    }
    public String getJiankongCount() {
        return jiankongCount;
    }

    public void setJiankongCount(String jiankongCount) {
        this.jiankongCount = jiankongCount;
    }
    public String getXiaofang() {
        return xiaofang;
    }

    public void setXiaofang(String xiaofang) {
        this.xiaofang = xiaofang;
    }
    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
    public String getCaijiType() {
        return caijiType;
    }

    public void setCaijiType(String caijiType) {
        this.caijiType = caijiType;
    }
    public String getCaijiAccount() {
        return caijiAccount;
    }

    public void setCaijiAccount(String caijiAccount) {
        this.caijiAccount = caijiAccount;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }

    public List<Pet> getPetList() {
        return petList;
    }

    public void setPetList(List<Pet> petList) {
        this.petList = petList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    @Override
    public String toString() {
        return "Room{" +
            "id=" + id +
            ", name=" + name +
            ", address=" + address +
            ", estateId=" + estateId +
            ", buildingId=" + buildingId +
            ", unitId=" + unitId +
            ", organizationId=" + organizationId +
            ", xingzhi=" + xingzhi +
            ", xingshi=" + xingshi +
            ", area=" + area +
            ", roomCount=" + roomCount +
            ", ownerName=" + ownerName +
            ", ownerCard=" + ownerCard +
            ", ownerPhone=" + ownerPhone +
            ", ownerAddress=" + ownerAddress +
            ", zulin=" + zulin +
            ", zhian=" + zhian +
            ", zujin=" + zujin +
            ", qizuriqi=" + qizuriqi +
            ", zuqi=" + zuqi +
            ", tuoguanName=" + tuoguanName +
            ", tuoguanPhone=" + tuoguanPhone +
            ", wurenji=" + wurenji +
            ", wurenjiXinghao=" + wurenjiXinghao +
            ", wurenjiXulie=" + wurenjiXulie +
            ", wurenjiOwner=" + wurenjiOwner +
            ", jiankong=" + jiankong +
            ", jiankongCount=" + jiankongCount +
            ", xiaofang=" + xiaofang +
            ", intro=" + intro +
            ", caijiType=" + caijiType +
            ", caijiAccount=" + caijiAccount +
            ", type=" + type +
        "}";
    }
}

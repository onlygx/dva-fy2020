package com.ow.dva.module.room.controller;


import com.ow.dva.module.city.entity.City;
import com.ow.dva.module.city.service.CityService;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.service.EstateService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 小区 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "小区 相关控制器")
@RequestMapping("/estate")
public class EstateController extends BaseController {

    /**
    * 分页获取小区数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取小区信息列表，分页，可携带条件")
    @RequiresPermissions("estate:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Estate> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Estate> page = estateService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "estate/list";
    }

    /**
     * 查询小区详情
     * 创建时间：2020-02-15 高祥
     * 修改时间：2020-02-15 高祥 加注释
     * @param id 小区ID
     */
    @GetMapping(value = "/selectById")
    @ResponseBody
    public RT selectById(String id){

        return RT.ok(estateService.selectById(id));
    }

    /**
    * 进入小区添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入小区添加页面")
    @RequiresPermissions("estate:add")
    @GetMapping(value = "/add")
    public String add(){

        return "estate/add";
    }

    /**
    * 添加小区信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个小区信息")
    @RequiresPermissions("estate:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Estate estate){

        return estateService.save(estate) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入小区编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入小区编辑页面")
    @RequiresPermissions("estate:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",estateService.getById(id));

        return "estate/edit";
    }

    /**
    * 修改小区信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个小区信息")
    @RequiresPermissions("estate:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Estate estate){

        return estateService.updateById(estate) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个小区信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个小区信息")
    @RequiresPermissions("estate:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的小区ID") String id){

        return estateService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个小区信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个小区信息")
    @RequiresPermissions("estate:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的小区ID List") @RequestParam List<String> ids){

        return estateService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @ApiOperation(value = "获取一级组织",notes = "获取全部一级组织")
    @RequiresPermissions("estate:page")
    @GetMapping(value = "/create")
    public String tree(String parentId, Model model){
        List<Organization> list = organizationService.findByParentId(parentId);
        List<City> cityList = cityService.selectProvince();
        model.addAttribute("list",list);
        model.addAttribute("parentId",parentId);
        model.addAttribute("cityList",cityList);
        return "estate/create";
    }


    @Resource
    EstateService estateService;

    @Resource
    OrganizationService organizationService;
    @Resource
    CityService cityService;
}

package com.ow.dva.module.room.service;

import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 住房 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface RoomService extends DvaService<Room> {

    /**
    * 分页获取住房数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Room> page(DvaPage<Room> page, Map<String,Object> param);


    /**
     * 描述： 添加房屋以及房屋相关详细信息
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    boolean saveRoomInfo(Room room);


    /**
     * @Author 闫增宝
     * @Description 根据房屋ID查询房屋详情
     * @Date 2020/2/15 23:50
     * @Param roomId 房间ID
     */
    Room selectRoomInfoById(String roomId);

    Room selectByNameAndBuilding(String buildingId, String name);

    Room selectById(String roomId);
}

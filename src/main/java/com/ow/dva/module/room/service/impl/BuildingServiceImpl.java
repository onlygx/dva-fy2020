package com.ow.dva.module.room.service.impl;

import com.ow.dva.module.room.entity.Building;
import com.ow.dva.module.room.mapper.BuildingMapper;
import com.ow.dva.module.room.service.BuildingService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 楼栋 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class BuildingServiceImpl extends DvaServiceImpl<BuildingMapper, Building> implements BuildingService {

    @DvaCache5M
    @Override
    public DvaPage<Building> page(DvaPage<Building> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public List<Building> selectByEstateId(String estateId) {
        return baseMapper.selectByEstateId(estateId);
    }
}

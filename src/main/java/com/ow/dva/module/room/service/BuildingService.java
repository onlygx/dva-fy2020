package com.ow.dva.module.room.service;

import com.ow.dva.module.room.entity.Building;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 楼栋 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface BuildingService extends DvaService<Building> {

    /**
    * 分页获取楼栋数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Building> page(DvaPage<Building> page, Map<String,Object> param);

    List<Building> selectByEstateId(String estateId);
}

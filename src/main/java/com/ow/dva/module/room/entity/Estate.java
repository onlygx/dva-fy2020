package com.ow.dva.module.room.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 小区
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_estate")
@ApiModel(value="Estate对象", description="小区")
public class Estate extends BaseEntity<Estate> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "小区名称")
    private String name;

    @ApiModelProperty(value = "小区地址")
    private String address;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Estate{" +
            "id=" + id +
            ", name=" + name +
            ", address=" + address +
            ", organizationId=" + organizationId +
        "}";
    }
}

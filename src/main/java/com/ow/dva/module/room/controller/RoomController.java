package com.ow.dva.module.room.controller;


import com.ow.dva.module.people.service.CarService;
import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.service.BuildingService;
import com.ow.dva.module.room.service.EstateService;
import com.ow.dva.module.room.service.UnitService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.room.service.RoomService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 住房 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "住房 相关控制器")
@RequestMapping("/room")
public class RoomController extends BaseController {

    /**
    * 分页获取住房数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取住房信息列表，分页，可携带条件")
    @RequiresPermissions("room:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Room> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Room> page = roomService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "room/list";
    }

    /**
    * 进入住房添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入住房添加页面")
    @RequiresPermissions("room:add")
    @GetMapping(value = "/add")
    public String add(Model model){
        List<Estate> estates = estateService.selectAll();
        model.addAttribute("estates",estates);
        return "room/add";
    }

    /**
    * 添加住房信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个住房信息")
    @RequiresPermissions("room:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Room room){
        Room countRoom = roomService.selectByNameAndBuilding(room.getBuildingId(),room.getName());
        if (countRoom==null){
            Estate estate = estateService.selectById(room.getEstateId());
            room.setOrganizationId(estate.getOrganizationId());
            return roomService.save(room) ? RT.ok() : RT.error(RT.ERROR_0101);
        }else {
            return RT.error(RT.ERROR_0101,"请勿重复添加房间");
        }

    }

    /**
    * 进入住房编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入住房编辑页面")
    @RequiresPermissions("room:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",roomService.getById(id));

        return "room/edit";
    }

    /**
    * 修改住房信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个住房信息")
    @RequiresPermissions("room:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Room room){

        return roomService.updateById(room) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个住房信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个住房信息")
    @RequiresPermissions("room:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的住房ID") String id){

        return roomService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个住房信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个住房信息")
    @RequiresPermissions("room:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的住房ID List") @RequestParam List<String> ids){

        return roomService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    /**
     * 进入房屋人口添加页面。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入添加页面",notes = "进入住房添加页面")
    @GetMapping(value = "/addPeople")
    public String addPeople(String roomId,Model model){
        Room room = roomService.selectById(roomId);
        model.addAttribute("roomId",roomId);
        model.addAttribute("orgId",room.getOrganizationId());
        return "room/addPeople";
    }

    /**
     * 进入住房宠物添加页面。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入添加页面",notes = "进入住房添加页面")
    @GetMapping(value = "/addPet")
    public String addPet(String roomId,Model model){
        Room room = roomService.selectById(roomId);
        model.addAttribute("roomId",roomId);
        model.addAttribute("orgId",room.getOrganizationId());
        return "room/addPet";
    }

    /**
     * 进入住房车辆添加页面。
     * 创建时间：2020-02-15 张仕济
     * 修改时间：2020-02-15 张仕济 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入添加页面",notes = "进入住房添加页面")
    @GetMapping(value = "/addCar")
    public String addCar(String roomId,Model model){
        Room room = roomService.selectById(roomId);
        model.addAttribute("roomId",roomId);
        model.addAttribute("orgId",room.getOrganizationId());
        return "room/addCar";
    }

//    /**
//     * 查看车辆页面。
//     * 创建时间：2020-02-15 张仕济
//     * 修改时间：2020-02-15 张仕济 加注释
//     * 修改时间：
//     */
//    @ApiOperation(value = "查看车辆页面",notes = "进入车辆编辑页面")
//    @GetMapping(value = "/lookCar")
//    public String lookCar(String id,Model model){
//        model.addAttribute("data",carService.getById(id));
//        return "room/lookCar";
//    }

    @Resource
    RoomService roomService;
    @Resource
    EstateService estateService;
    @Resource
    BuildingService buildingService;
    @Resource
    UnitService unitService;
    @Resource
    CarService carService;
}

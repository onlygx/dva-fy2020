package com.ow.dva.module.room.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 单元
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_unit")
@ApiModel(value="Unit对象", description="单元")
public class Unit extends BaseEntity<Unit> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "单元名称")
    private String name;

    @ApiModelProperty(value = "所属小区ID")
    private String estateId;

    @ApiModelProperty(value = "所属楼栋ID")
    private String buildingId;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEstateId() {
        return estateId;
    }

    public void setEstateId(String estateId) {
        this.estateId = estateId;
    }
    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Unit{" +
            "id=" + id +
            ", name=" + name +
            ", estateId=" + estateId +
            ", buildingId=" + buildingId +
            ", organizationId=" + organizationId +
        "}";
    }
}

package com.ow.dva.module.shop.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.service.CarService;
import com.ow.dva.module.people.service.PeopleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.shop.entity.Shop;
import com.ow.dva.module.shop.service.ShopService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 门店 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "门店 相关控制器")
@RequestMapping("/shop")
public class ShopController extends BaseController {

    /**
    * 分页获取门店数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取门店信息列表，分页，可携带条件")
    @RequiresPermissions("shop:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Shop> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Shop> page = shopService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "shop/list";
    }

    /**
    * 进入门店添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入门店添加页面")
    @RequiresPermissions("shop:add")
    @GetMapping(value = "/add")
    public String add(Model model){

        String parentId = "0,";
        List<Organization> oraList = organizationService.findByParentId(parentId);
        model.addAttribute("oraList",oraList);
        model.addAttribute("parentId",parentId);
        return "shop/add";
    }

    /**
    * 添加门店信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个门店信息")
    @RequiresPermissions("shop:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(@SessionAttribute(value="account") Account account,Shop shop){
        shop.setAccountId(account.getId());

        if(account.getType().contains("user")){
            shop.setStatus(0);
        }else{
            shop.setStatus(1);
        }
        return shopService.save(shop) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入门店编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入门店编辑页面")
    @RequiresPermissions("shop:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",shopService.getById(id));

        return "shop/edit";
    }

    /**
    * 修改门店信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个门店信息")
    @RequiresPermissions("shop:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Shop shop){

        return shopService.updateById(shop) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个门店信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个门店信息")
    @RequiresPermissions("shop:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的门店ID") String id){

        return shopService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个门店信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个门店信息")
    @RequiresPermissions("shop:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的门店ID List") @RequestParam List<String> ids){

        return shopService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }

    @GetMapping("/addPer")
    public String addPer(String id, String organizationId, Model model){
        model.addAttribute("shopId",id);
        model.addAttribute("organizationId",organizationId);
        return "shop/addPer";
    }

    @GetMapping("/addCar")
    public String addCar(String id, String organizationId, Model model){
        model.addAttribute("shopId",id);
        model.addAttribute("organizationId",organizationId);
        return "shop/addCar";
    }

    @GetMapping("/openPerList")

    public String openPerList(DvaPage<People> dvaPage, @RequestParam Map<String, Object> param, String id, Model model){

        param.put("fromType","shop");
        param.put("fromId",id);
        DvaPage<People> page  = peopleService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);

        return "shop/personnelList";
    }

    @GetMapping("/openCarList")

    public String openCarList(DvaPage<Car> dvaPage, @RequestParam Map<String, Object> param, String id, Model model){

        param.put("fromType","shop");
        param.put("fromId",id);
        DvaPage<Car> page  = carService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);

        return "shop/carList";
    }

    @Resource
    ShopService shopService;
    @Resource
    OrganizationService organizationService;
    @Resource
    PeopleService peopleService;
    @Resource
    CarService carService;
}

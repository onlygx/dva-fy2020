package com.ow.dva.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.image.service.ImagesService;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.mapper.CarMapper;
import com.ow.dva.module.people.mapper.PeopleMapper;
import com.ow.dva.module.shop.entity.Shop;
import com.ow.dva.module.shop.mapper.ShopMapper;
import com.ow.dva.module.shop.service.ShopService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 门店 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class ShopServiceImpl extends DvaServiceImpl<ShopMapper, Shop> implements ShopService {

    @Resource
    PeopleMapper peopleMapper;
    @Resource
    CarMapper carMapper;
    @Resource
    ImagesService imagesService;

    @DvaCache5M
    @Override
    public DvaPage<Shop> page(DvaPage<Shop> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }


    @Override
    public boolean saveShopInfo(Shop shop) {
        int flag = baseMapper.insert(shop);
        if(flag > 0){
            //门店下的人员添加
            savePeople(shop);
            //门店下的车辆添加
            saveCar(shop);
            //添加图片
            imagesService.saveShopImege(shop);
            return true;
        }else{
            return false;
        }
    }

    //    门店下的人员添加
    private void savePeople(Shop shop){
        List<People> peopleList = shop.getPeopleList();
        if (peopleList != null && !peopleList.isEmpty()){
            for (People people: peopleList) {
                people.setFromType("shop");
                people.setFromId(shop.getId());
                people.setOrganizationId(shop.getOrganizationId());
                peopleMapper.insert(people);
                imagesService.savePeopleImage(people);
            }
        }
    }

    //    门店下的车辆添加
    private void saveCar(Shop shop){
        List<Car> carList = shop.getCarList();
        if (carList != null && !carList.isEmpty()){
            for (Car car: carList) {
                car.setFromType("shop");
                car.setFromId(shop.getId());
                car.setOrganizationId(shop.getOrganizationId());
                carMapper.insert(car);
                imagesService.saveCarImage(car);
            }
        }
    }

    @Override
    public Shop selectShopInfo(String shopId) {
        Shop shop = baseMapper.selectById(shopId);
        if(shop != null){
            //查询人
            selectPeople(shop);
            //查询车辆
            selectCar(shop);

            shop.setImageList(imagesService.selectByTypeFrom("shop",shop.getId()));
        }
        return shop;
    }

    private void selectPeople(Shop room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","shop");
        queryWrapper.eq("from_id",room.getId());
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(people -> {
            people.setImageList(imagesService.selectByTypeFrom("people",people.getId()));
        });
        room.setPeopleList(peopleList);
    }

    private void selectCar(Shop room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","shop");
        queryWrapper.eq("from_id",room.getId());
        List<Car> carList = carMapper.selectList(queryWrapper);
        carList.forEach(data -> {
            data.setImageList(imagesService.selectByTypeFrom("car",data.getId()));
        });
        room.setCarList(carList);
    }

}

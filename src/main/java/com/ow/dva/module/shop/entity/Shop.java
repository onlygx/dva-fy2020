package com.ow.dva.module.shop.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 门店
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_shop")
@ApiModel(value="Shop对象", description="门店")
public class Shop extends BaseEntity<Shop> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "门店名称")
    private String name;

    @ApiModelProperty(value = "门店地址")
    private String address;

    @ApiModelProperty(value = "门店类型")
    private String type;

    @ApiModelProperty(value = "信用代码")
    private String number;

    @ApiModelProperty(value = "法人姓名")
    private String ownerName;

    @ApiModelProperty(value = "法人身份证")
    private String ownerCard;

    @ApiModelProperty(value = "法人手机号")
    private String ownerPhone;

    @ApiModelProperty(value = "面积")
    private String area;

    @ApiModelProperty(value = "房间数")
    private String roomCount;

    @ApiModelProperty(value = "监控数量")
    private String jkCount;

    @ApiModelProperty(value = "红外/报警器数量")
    private String bjCount;

    @ApiModelProperty(value = "一键报警数量")
    private String yjbjCount;

    @ApiModelProperty(value = "整合入公安机关数量")
    private String zhrgajgCount;

    @ApiModelProperty(value = "防盗门")
    private String fdmCount;

    @ApiModelProperty(value = "防盗窗")
    private String fdcCount;

    @ApiModelProperty(value = "消防栓")
    private String xfsCount;

    @ApiModelProperty(value = "灭火器")
    private String mhqCount;

    @ApiModelProperty(value = "烟感报警器")
    private String ygbjqCount;

    @ApiModelProperty(value = "疏散通道")
    private String ssCount;

    @ApiModelProperty(value = "是否租赁")
    private String zulin;

    @ApiModelProperty(value = "租赁房主")
    private String zulinOwner;

    @ApiModelProperty(value = "房主手机号")
    private String zulinPhone;

    @ApiModelProperty(value = "网络接入情况")
    private String web;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @ApiModelProperty(value = "采集类型")
    private String caijiType;

    @ApiModelProperty(value = "采集人员")
    private String caijiAccount;

    @ApiModelProperty(value = "审核状态")
    private int status;

    @TableField(exist = false)
    private String images;

    @ApiModelProperty(value = "录入用户")
    private String accountId;
    /**
     * 自定义 人员集合
     */
    @TableField(exist = false)
    private List<People> peopleList;

    /**
     * 自定义 车辆集合
     */
    @TableField(exist = false)
    private List<Car> carList;
    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }
    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    public String getOwnerCard() {
        return ownerCard;
    }

    public void setOwnerCard(String ownerCard) {
        this.ownerCard = ownerCard;
    }
    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    public String getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(String roomCount) {
        this.roomCount = roomCount;
    }
    public String getJkCount() {
        return jkCount;
    }

    public void setJkCount(String jkCount) {
        this.jkCount = jkCount;
    }
    public String getBjCount() {
        return bjCount;
    }

    public void setBjCount(String bjCount) {
        this.bjCount = bjCount;
    }
    public String getYjbjCount() {
        return yjbjCount;
    }

    public void setYjbjCount(String yjbjCount) {
        this.yjbjCount = yjbjCount;
    }
    public String getZhrgajgCount() {
        return zhrgajgCount;
    }

    public void setZhrgajgCount(String zhrgajgCount) {
        this.zhrgajgCount = zhrgajgCount;
    }
    public String getFdmCount() {
        return fdmCount;
    }

    public void setFdmCount(String fdmCount) {
        this.fdmCount = fdmCount;
    }
    public String getFdcCount() {
        return fdcCount;
    }

    public void setFdcCount(String fdcCount) {
        this.fdcCount = fdcCount;
    }
    public String getXfsCount() {
        return xfsCount;
    }

    public void setXfsCount(String xfsCount) {
        this.xfsCount = xfsCount;
    }
    public String getMhqCount() {
        return mhqCount;
    }

    public void setMhqCount(String mhqCount) {
        this.mhqCount = mhqCount;
    }
    public String getYgbjqCount() {
        return ygbjqCount;
    }

    public void setYgbjqCount(String ygbjqCount) {
        this.ygbjqCount = ygbjqCount;
    }
    public String getSsCount() {
        return ssCount;
    }

    public void setSsCount(String ssCount) {
        this.ssCount = ssCount;
    }
    public String getZulin() {
        return zulin;
    }

    public void setZulin(String zulin) {
        this.zulin = zulin;
    }
    public String getZulinOwner() {
        return zulinOwner;
    }

    public void setZulinOwner(String zulinOwner) {
        this.zulinOwner = zulinOwner;
    }
    public String getZulinPhone() {
        return zulinPhone;
    }

    public void setZulinPhone(String zulinPhone) {
        this.zulinPhone = zulinPhone;
    }
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
    public String getCaijiType() {
        return caijiType;
    }

    public void setCaijiType(String caijiType) {
        this.caijiType = caijiType;
    }
    public String getCaijiAccount() {
        return caijiAccount;
    }

    public void setCaijiAccount(String caijiAccount) {
        this.caijiAccount = caijiAccount;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "Shop{" +
            "id=" + id +
            ", name=" + name +
            ", address=" + address +
            ", type=" + type +
            ", number=" + number +
            ", ownerName=" + ownerName +
            ", ownerCard=" + ownerCard +
            ", ownerPhone=" + ownerPhone +
            ", area=" + area +
            ", roomCount=" + roomCount +
            ", jkCount=" + jkCount +
            ", bjCount=" + bjCount +
            ", yjbjCount=" + yjbjCount +
            ", zhrgajgCount=" + zhrgajgCount +
            ", fdmCount=" + fdmCount +
            ", fdcCount=" + fdcCount +
            ", xfsCount=" + xfsCount +
            ", mhqCount=" + mhqCount +
            ", ygbjqCount=" + ygbjqCount +
            ", ssCount=" + ssCount +
            ", zulin=" + zulin +
            ", zulinOwner=" + zulinOwner +
            ", zulinPhone=" + zulinPhone +
            ", web=" + web +
            ", organizationId=" + organizationId +
            ", caijiType=" + caijiType +
            ", caijiAccount=" + caijiAccount +
        "}";
    }
}

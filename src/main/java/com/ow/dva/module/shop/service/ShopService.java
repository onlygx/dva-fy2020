package com.ow.dva.module.shop.service;

import com.ow.dva.module.shop.entity.Shop;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 门店 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface ShopService extends DvaService<Shop> {

    /**
    * 分页获取门店数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Shop> page(DvaPage<Shop> page, Map<String,Object> param);

    /**
     * 描述：门店相关信息添加
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    boolean saveShopInfo(Shop shop);
    
    /**
     * @Author 闫增宝
     * @Description 根据门店ID查询门店详情
     * @Date 2020/2/16 0:27 
     * @Param shopId 门店ID
     */
    Shop selectShopInfo(String shopId);
}

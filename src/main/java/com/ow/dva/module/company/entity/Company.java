package com.ow.dva.module.company.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 单位
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_company")
@ApiModel(value="Company对象", description="单位")
public class Company extends BaseEntity<Company> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "公司名称")
    private String name;

    @ApiModelProperty(value = "公司地址")
    private String address;

    @ApiModelProperty(value = "单位类别")
    private String type;

    @ApiModelProperty(value = "单位性质")
    private String xingzhi;

    @ApiModelProperty(value = "重点等级")
    private String level;

    @ApiModelProperty(value = "信用代码")
    private String number;

    @ApiModelProperty(value = "法人姓名")
    private String ownerName;

    @ApiModelProperty(value = "法人身份证")
    private String ownerCard;

    @ApiModelProperty(value = "法人手机号")
    private String ownerPhone;

    @ApiModelProperty(value = "单位面积")
    private String area;

    @ApiModelProperty(value = "楼栋数")
    private String buildingCount;

    @ApiModelProperty(value = "房间数")
    private String roomCount;

    @ApiModelProperty(value = "床位数")
    private String bedCount;

    @ApiModelProperty(value = "安保人员")
    private String anbaoName;

    @ApiModelProperty(value = "安保人员身份证")
    private String anbaoCard;

    @ApiModelProperty(value = "安保人员手机")
    private String anbaoPhone;

    @ApiModelProperty(value = "要害部位")
    private String yaohai;

    @ApiModelProperty(value = "监控数量")
    private String jkCount;

    @ApiModelProperty(value = "红外/报警器数量")
    private String bjCount;

    @ApiModelProperty(value = "一键报警数量")
    private String yjbjCount;

    @ApiModelProperty(value = "整合入公安机关数量")
    private String zhrgajgCount;

    @ApiModelProperty(value = "防盗门")
    private String fdmCount;

    @ApiModelProperty(value = "防盗窗")
    private String fdcCount;

    @ApiModelProperty(value = "消防栓")
    private String xfsCount;

    @ApiModelProperty(value = "火眼报警系统")
    private String hybjxtCount;

    @ApiModelProperty(value = "灭火器")
    private String mhqCount;

    @ApiModelProperty(value = "烟感报警器")
    private String ygbjqCount;

    @ApiModelProperty(value = "网络接入情况")
    private String web;

    @ApiModelProperty(value = "干部数量")
    private String gbCount;

    @ApiModelProperty(value = "工人数量")
    private String grCount;

    @ApiModelProperty(value = "临时工数量")
    private String lsgrCount;

    @ApiModelProperty(value = "保安服务类型")
    private String baType;

    @ApiModelProperty(value = "保安公司名称")
    private String baCompany;

    @ApiModelProperty(value = "保安数量")
    private String baCount;

    @ApiModelProperty(value = "专职保安数量")
    private String baZzCount;

    @ApiModelProperty(value = "兼职保安数量")
    private String baJzCount;

    @ApiModelProperty(value = "应急预案")
    private String baYingji;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @ApiModelProperty(value = "采集类型")
    private String caijiType;

    @ApiModelProperty(value = "采集人员")
    private String caijiAccount;
    @ApiModelProperty(value = "审核状态")
    private int status;

    @TableField(exist = false)
    private String images;

    @ApiModelProperty(value = "录入用户")
    private String accountId;

    /**
     * 自定义 人员集合
     */
    @TableField(exist = false)
    private List<People> peopleList;

    /**
     * 自定义 车辆集合
     */
    @TableField(exist = false)
    private List<Car> carList;

    /**
     * 自定义 宠物集合
     */
    @TableField(exist = false)
    private List<Pet> petList;

    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getXingzhi() {
        return xingzhi;
    }

    public void setXingzhi(String xingzhi) {
        this.xingzhi = xingzhi;
    }
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    public String getOwnerCard() {
        return ownerCard;
    }

    public void setOwnerCard(String ownerCard) {
        this.ownerCard = ownerCard;
    }
    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
    public String getBuildingCount() {
        return buildingCount;
    }

    public void setBuildingCount(String buildingCount) {
        this.buildingCount = buildingCount;
    }
    public String getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(String roomCount) {
        this.roomCount = roomCount;
    }
    public String getBedCount() {
        return bedCount;
    }

    public void setBedCount(String bedCount) {
        this.bedCount = bedCount;
    }
    public String getAnbaoName() {
        return anbaoName;
    }

    public void setAnbaoName(String anbaoName) {
        this.anbaoName = anbaoName;
    }
    public String getAnbaoCard() {
        return anbaoCard;
    }

    public void setAnbaoCard(String anbaoCard) {
        this.anbaoCard = anbaoCard;
    }
    public String getAnbaoPhone() {
        return anbaoPhone;
    }

    public void setAnbaoPhone(String anbaoPhone) {
        this.anbaoPhone = anbaoPhone;
    }
    public String getYaohai() {
        return yaohai;
    }

    public void setYaohai(String yaohai) {
        this.yaohai = yaohai;
    }
    public String getJkCount() {
        return jkCount;
    }

    public void setJkCount(String jkCount) {
        this.jkCount = jkCount;
    }
    public String getBjCount() {
        return bjCount;
    }

    public void setBjCount(String bjCount) {
        this.bjCount = bjCount;
    }
    public String getYjbjCount() {
        return yjbjCount;
    }

    public void setYjbjCount(String yjbjCount) {
        this.yjbjCount = yjbjCount;
    }
    public String getZhrgajgCount() {
        return zhrgajgCount;
    }

    public void setZhrgajgCount(String zhrgajgCount) {
        this.zhrgajgCount = zhrgajgCount;
    }
    public String getFdmCount() {
        return fdmCount;
    }

    public void setFdmCount(String fdmCount) {
        this.fdmCount = fdmCount;
    }
    public String getFdcCount() {
        return fdcCount;
    }

    public void setFdcCount(String fdcCount) {
        this.fdcCount = fdcCount;
    }
    public String getXfsCount() {
        return xfsCount;
    }

    public void setXfsCount(String xfsCount) {
        this.xfsCount = xfsCount;
    }
    public String getHybjxtCount() {
        return hybjxtCount;
    }

    public void setHybjxtCount(String hybjxtCount) {
        this.hybjxtCount = hybjxtCount;
    }
    public String getMhqCount() {
        return mhqCount;
    }

    public void setMhqCount(String mhqCount) {
        this.mhqCount = mhqCount;
    }
    public String getYgbjqCount() {
        return ygbjqCount;
    }

    public void setYgbjqCount(String ygbjqCount) {
        this.ygbjqCount = ygbjqCount;
    }
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }
    public String getGbCount() {
        return gbCount;
    }

    public void setGbCount(String gbCount) {
        this.gbCount = gbCount;
    }
    public String getGrCount() {
        return grCount;
    }

    public void setGrCount(String grCount) {
        this.grCount = grCount;
    }
    public String getLsgrCount() {
        return lsgrCount;
    }

    public void setLsgrCount(String lsgrCount) {
        this.lsgrCount = lsgrCount;
    }
    public String getBaType() {
        return baType;
    }

    public void setBaType(String baType) {
        this.baType = baType;
    }
    public String getBaCompany() {
        return baCompany;
    }

    public void setBaCompany(String baCompany) {
        this.baCompany = baCompany;
    }
    public String getBaCount() {
        return baCount;
    }

    public void setBaCount(String baCount) {
        this.baCount = baCount;
    }
    public String getBaZzCount() {
        return baZzCount;
    }

    public void setBaZzCount(String baZzCount) {
        this.baZzCount = baZzCount;
    }
    public String getBaJzCount() {
        return baJzCount;
    }

    public void setBaJzCount(String baJzCount) {
        this.baJzCount = baJzCount;
    }
    public String getBaYingji() {
        return baYingji;
    }

    public void setBaYingji(String baYingji) {
        this.baYingji = baYingji;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
    public String getCaijiType() {
        return caijiType;
    }

    public void setCaijiType(String caijiType) {
        this.caijiType = caijiType;
    }
    public String getCaijiAccount() {
        return caijiAccount;
    }

    public void setCaijiAccount(String caijiAccount) {
        this.caijiAccount = caijiAccount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    public List<People> getPeopleList() {
        return peopleList;
    }

    public void setPeopleList(List<People> peopleList) {
        this.peopleList = peopleList;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    public List<Pet> getPetList() {
        return petList;
    }

    public void setPetList(List<Pet> petList) {
        this.petList = petList;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Company{" +
            "id=" + id +
            ", name=" + name +
            ", address=" + address +
            ", type=" + type +
            ", xingzhi=" + xingzhi +
            ", level=" + level +
            ", number=" + number +
            ", ownerName=" + ownerName +
            ", ownerCard=" + ownerCard +
            ", ownerPhone=" + ownerPhone +
            ", area=" + area +
            ", buildingCount=" + buildingCount +
            ", roomCount=" + roomCount +
            ", bedCount=" + bedCount +
            ", anbaoName=" + anbaoName +
            ", anbaoCard=" + anbaoCard +
            ", anbaoPhone=" + anbaoPhone +
            ", yaohai=" + yaohai +
            ", jkCount=" + jkCount +
            ", bjCount=" + bjCount +
            ", yjbjCount=" + yjbjCount +
            ", zhrgajgCount=" + zhrgajgCount +
            ", fdmCount=" + fdmCount +
            ", fdcCount=" + fdcCount +
            ", xfsCount=" + xfsCount +
            ", hybjxtCount=" + hybjxtCount +
            ", mhqCount=" + mhqCount +
            ", ygbjqCount=" + ygbjqCount +
            ", web=" + web +
            ", gbCount=" + gbCount +
            ", grCount=" + grCount +
            ", lsgrCount=" + lsgrCount +
            ", baType=" + baType +
            ", baCompany=" + baCompany +
            ", baCount=" + baCount +
            ", baZzCount=" + baZzCount +
            ", baJzCount=" + baJzCount +
            ", baYingji=" + baYingji +
            ", organizationId=" + organizationId +
            ", caijiType=" + caijiType +
            ", caijiAccount=" + caijiAccount +
        "}";
    }
}

package com.ow.dva.module.company.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.company.mapper.CompanyMapper;
import com.ow.dva.module.company.service.CompanyService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.image.service.ImagesService;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.people.mapper.CarMapper;
import com.ow.dva.module.people.mapper.PeopleMapper;
import com.ow.dva.module.people.mapper.PetMapper;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单位 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class CompanyServiceImpl extends DvaServiceImpl<CompanyMapper, Company> implements CompanyService {


    @Resource
    PeopleMapper peopleMapper;
    @Resource
    CarMapper carMapper;
    @Resource
    PetMapper petMapper;

    @Resource
    ImagesService imagesService;

    @DvaCache5M
    @Override
    public DvaPage<Company> page(DvaPage<Company> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public boolean saveCompanyInfo(Company company) {

        int flag = baseMapper.insert(company);
        if(flag > 0){
            //单位下的人员添加
            savePeople(company);
            //单位下的车辆添加
            saveCar(company);
            //单位下的宠物添加
            savePet(company);
            //添加图片
            imagesService.saveCompanyImege(company);
            return true;
        }else{
            return false;
        }
    }

    //    单位下的人员添加
    private void savePeople(Company company){
        List<People> peopleList = company.getPeopleList();
        if (peopleList != null && !peopleList.isEmpty()){
            for (People people: peopleList) {
                people.setFromType("company");
                people.setFromId(company.getId());
                people.setOrganizationId(company.getOrganizationId());
                peopleMapper.insert(people);
                imagesService.savePeopleImage(people);
            }
        }
    }


    //    单位下的车辆添加
    private void saveCar(Company company){
        List<Car> carList = company.getCarList();
        if (carList != null && !carList.isEmpty()){
            for (Car car: carList) {
                car.setFromType("company");
                car.setFromId(company.getId());
                car.setOrganizationId(company.getOrganizationId());
                carMapper.insert(car);
                imagesService.saveCarImage(car);
            }
        }
    }

//    单位下的宠物添加
    private void savePet(Company room){
        List<Pet> petList = room.getPetList();
        if (petList != null && !petList.isEmpty()){
            for (Pet pet: petList) {
                pet.setFromType("company");
                pet.setFromId(room.getId());
                pet.setOrganizationId(room.getOrganizationId());
                petMapper.insert(pet);
                imagesService.savePetImage(pet);
            }
        }
    }

    @Override
    public Company selectCompanyInfo(String companyId) {
        Company company = baseMapper.selectById(companyId);
        if(company != null){
            //查询人
            selectPeople(company);
            //查询车辆
            selectCar(company);
            //查询宠物
            selectPet(company);

            company.setImageList(imagesService.selectByTypeFrom("company",company.getId()));
        }
        return company;
    }

    private void selectPeople(Company company){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","company");
        queryWrapper.eq("from_id",company.getId());
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(people -> {
            people.setImageList(imagesService.selectByTypeFrom("people",people.getId()));
        });
        company.setPeopleList(peopleList);
    }

    private void selectCar(Company room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","company");
        queryWrapper.eq("from_id",room.getId());
        List<Car> carList = carMapper.selectList(queryWrapper);
        carList.forEach(data -> {
            data.setImageList(imagesService.selectByTypeFrom("car",data.getId()));
        });
        room.setCarList(carList);
    }

    private void selectPet(Company room){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("from_type","company");
        queryWrapper.eq("from_id",room.getId());
        List<Pet> petList = petMapper.selectList(queryWrapper);
        petList.forEach(data -> {
            data.setImageList(imagesService.selectByTypeFrom("pet",data.getId()));
        });
        room.setPetList(petList);
    }
}

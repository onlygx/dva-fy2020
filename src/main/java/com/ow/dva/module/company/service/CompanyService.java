package com.ow.dva.module.company.service;

import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 单位 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface CompanyService extends DvaService<Company> {

    /**
    * 分页获取单位数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Company> page(DvaPage<Company> page, Map<String,Object> param);

    /**
     * 描述：单位详细信息添加
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    boolean saveCompanyInfo(Company company);


    /**
     * @Author 闫增宝
     * @Description 根据单位ID查询单位详情
     * @Date 2020/2/16 0:17
     * @Param companyId 单位ID
     */
    Company selectCompanyInfo(String companyId);
}

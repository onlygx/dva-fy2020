package com.ow.dva.module.company.controller;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.service.CarService;
import com.ow.dva.module.people.service.PeopleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.company.service.CompanyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 单位 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "单位 相关控制器")
@RequestMapping("/company")
public class CompanyController extends BaseController {

    /**
    * 分页获取单位数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取单位信息列表，分页，可携带条件")
    @RequiresPermissions("company:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Company> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Company> page = companyService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "company/list";
    }

    /**
    * 进入单位添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入单位添加页面")
    @RequiresPermissions("company:add")
    @GetMapping(value = "/add")
    public String add(Model model){

        String parentId = "0,";
        List<Organization> oraList = organizationService.findByParentId(parentId);
        model.addAttribute("oraList",oraList);
        model.addAttribute("parentId",parentId);
        return "company/add";
    }

    /**
    * 添加单位信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个单位信息")
    @RequiresPermissions("company:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(@SessionAttribute(value="account") Account account ,Company company){
        company.setAccountId(account.getId());
        if(account.getType().contains("user")){
            company.setStatus(0);
        }else{
            company.setStatus(1);
        }
        return companyService.save(company) ? RT.ok() : RT.error(RT.ERROR_0101);
    }


    /**
    * 进入单位编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入单位编辑页面")
    @RequiresPermissions("company:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",companyService.getById(id));

        return "company/edit";
    }

    /**
    * 修改单位信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个单位信息")
    @RequiresPermissions("company:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Company company){

        return companyService.updateById(company) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个单位信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个单位信息")
    @RequiresPermissions("company:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的单位ID") String id){

        return companyService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个单位信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个单位信息")
    @RequiresPermissions("company:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的单位ID List") @RequestParam List<String> ids){

        return companyService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }

    @GetMapping("/addPer")
    public String addPer(String id, String organizationId, Model model){
        model.addAttribute("companyId",id);
        model.addAttribute("organizationId",organizationId);
        return "company/addPer";
    }

    @GetMapping("/addCar")
    public String addCar(String id, String organizationId, Model model){
        model.addAttribute("companyId",id);
        model.addAttribute("organizationId",organizationId);
        return "company/addCar";
    }

    @GetMapping("/openPerList")

    public String openPerList(DvaPage<People> dvaPage, @RequestParam Map<String, Object> param, String id, Model model){

        param.put("fromType","company");
        param.put("fromId",id);
        DvaPage<People> page  = peopleService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);

        return "company/personnelList";
    }

    @GetMapping("/openCarList")

    public String openCarList(DvaPage<Car> dvaPage, @RequestParam Map<String, Object> param, String id, Model model){

        param.put("fromType","company");
        param.put("fromId",id);
        DvaPage<Car> page  = carService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);

        return "company/carList";
    }

    @Resource
    CompanyService companyService;
    @Resource
    OrganizationService organizationService;
    @Resource
    PeopleService peopleService;
    @Resource
    CarService carService;
}

package com.ow.dva.module.log.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.log.entity.Log;
import com.ow.dva.module.log.service.LogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 日志 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-15
 * @version V1.0
 */
@Controller
@Api(tags = "日志 相关控制器")
@RequestMapping("/log")
public class LogController extends BaseController {

    /**
    * 分页获取日志数据列表信息
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取日志信息列表，分页，可携带条件")
    @RequiresPermissions("log:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Log> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Log> page = logService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "log/list";
    }

    /**
    * 进入日志添加页面。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入日志添加页面")
    @RequiresPermissions("log:add")
    @GetMapping(value = "/add")
    public String add(){

        return "log/add";
    }

    /**
    * 添加日志信息。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个日志信息")
    @RequiresPermissions("log:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Log log){

        return logService.save(log) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入日志编辑页面。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入日志编辑页面")
    @RequiresPermissions("log:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",logService.getById(id));

        return "log/edit";
    }

    /**
    * 修改日志信息。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个日志信息")
    @RequiresPermissions("log:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Log log){

        return logService.updateById(log) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个日志信息。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个日志信息")
    @RequiresPermissions("log:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的日志ID") String id){

        return logService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个日志信息。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个日志信息")
    @RequiresPermissions("log:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的日志ID List") @RequestParam List<String> ids){

        return logService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    LogService logService;
}

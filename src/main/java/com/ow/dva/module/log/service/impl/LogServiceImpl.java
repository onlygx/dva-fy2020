package com.ow.dva.module.log.service.impl;

import com.ow.dva.module.log.entity.Log;
import com.ow.dva.module.log.mapper.LogMapper;
import com.ow.dva.module.log.service.LogService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 日志 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-15
 * @version V1.0
 */
@Service
public class LogServiceImpl extends DvaServiceImpl<LogMapper, Log> implements LogService {

    @DvaCache5M
    @Override
    public DvaPage<Log> page(DvaPage<Log> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }
}

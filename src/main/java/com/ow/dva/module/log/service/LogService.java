package com.ow.dva.module.log.service;

import com.ow.dva.module.log.entity.Log;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 日志 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-15
 * @version V1.0
 */
public interface LogService extends DvaService<Log> {

    /**
    * 分页获取日志数据列表信息。
    * 创建时间：2020-01-15 高祥
    * 修改时间：2020-01-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Log> page(DvaPage<Log> page, Map<String,Object> param);
}

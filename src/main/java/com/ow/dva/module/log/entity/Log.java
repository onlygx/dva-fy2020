package com.ow.dva.module.log.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 日志
 * </p>
 *
 * @author 高祥
 * @since 2020-01-15
 * @version V1.0
 */
@TableName("dva_log")
@ApiModel(value="Log对象", description="日志")
public class Log extends BaseEntity<Log> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "日志内容")
    private String content;

    @ApiModelProperty(value = "日志类型")
    private Integer type;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    @ApiModelProperty(value = "账号ID")
    private String accountId;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "IP地址")
    private String ip;

    @ApiModelProperty(value = "资源路径")
    private String method;

    @ApiModelProperty(value = "参数")
    private String params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Log{" +
            "id=" + id +
            ", content=" + content +
            ", type=" + type +
            ", createTime=" + createTime +
            ", accountId=" + accountId +
            ", status=" + status +
            ", ip=" + ip +
            ", method=" + method +
            ", params=" + params +
        "}";
    }
}

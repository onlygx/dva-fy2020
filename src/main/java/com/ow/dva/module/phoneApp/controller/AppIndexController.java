package com.ow.dva.module.phoneApp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.hutool.core.lang.UUID;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ow.dva.config.constant.UserConstant;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.company.service.CompanyService;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.room.service.EstateService;
import com.ow.dva.module.room.service.RoomService;
import com.ow.dva.module.shop.entity.Shop;
import com.ow.dva.module.shop.service.ShopService;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@Api(tags = "管理员 相关控制器")
@RequestMapping("/app/index")
public class AppIndexController extends BaseController {

    @GetMapping("")
    public String index(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        model.addAttribute("account",account);
        logger.info("访问手机页面");
        return "phoneApp/index";
    }
    @GetMapping("/userIndex")
    public String userIndex(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        model.addAttribute("account",account);
        logger.info("访问手机页面");
        return "phoneApp/userIndex";
    }
    @GetMapping("/roomAdd")
    public String roomAdd(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        model.addAttribute("strId", UUID.fastUUID().toString().replaceAll("-",""));
        return "phoneApp/roomAdd";
    }

    @GetMapping("/companyAdd")
    public String companyAdd(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        model.addAttribute("strId", UUID.fastUUID().toString().replaceAll("-",""));
        return "phoneApp/companyAdd";
    }

    @GetMapping("/shopAdd")
    public String shopAdd(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        model.addAttribute("strId", UUID.fastUUID().toString().replaceAll("-",""));
        return "phoneApp/shopAdd";
    }


    @GetMapping("/roomShow")
    public String roomShow(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        return "phoneApp/roomShow";
    }

    @GetMapping("/roomShowInfo")
    public String roomShowInfo(String roomId, Model model){

        Room room = roomService.selectRoomInfoById(roomId);
        model.addAttribute("data",room);
        return "phoneApp/roomShowInfo";
    }

    @GetMapping("/companyShow")
    public String companyShow(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        return "phoneApp/companyShow";
    }

    @GetMapping("/companyShowInfo")
    public String companyShowInfo(String companyId, Model model){
        Company company = companyService.selectCompanyInfo(companyId);
        model.addAttribute("data",company);
        return "phoneApp/companyShowInfo";
    }

    @GetMapping("/shopShow")
    public String shopShow(HttpSession session, Model model){
        Account account = null;
        try {
            account = (Account) session.getAttribute(UserConstant.ACCOUNT);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        model.addAttribute("account",account);
        model.addAttribute("organizationId",account.getOrganizationId());
        return "phoneApp/shopShow";
    }

    @GetMapping("/shopShowInfo")
    public String shopShow(String shopId, Model model){
        Shop shop = shopService.selectShopInfo(shopId);
        model.addAttribute("data",shop);
        return "phoneApp/shopShowInfo";
    }
    @GetMapping("/regShow")
    public String regShow(String orgId, Model model){
        Organization organization = organizationService.getById(orgId);
        model.addAttribute("data",organization);
        return "phoneApp/reg";
    }

    @Resource
    EstateService estateService;

    @Resource
    RoomService roomService;

    @Resource
    CompanyService companyService;

    @Resource
    ShopService shopService;
    @Resource
    OrganizationService organizationService;
}

package com.ow.dva.module.phoneApp.controller;


import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.account.service.AccountService;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.shop.entity.Shop;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@Api(tags = "注册 相关控制器")
@RequestMapping("/app/reg")
public class AppRegController extends BaseController {
    /**
     * 描述：添加门店以及门店相关详细信息
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @PostMapping(value = "/saveAccountUser")
    @ApiOperation(value = "添加门店以及门店相关详细信息",notes = "添加门店以及门店相关详细信息")
    @ResponseBody
    public RT saveShopInfo(Account account){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_name",account.getUserName());
        int count = accountService.count(queryWrapper);
        if(count==0){
            Organization organization = organizationService.getById(account.getOrganizationId());

            account.setStatus(1);
            account.setType("admin,user,");
            account.setOrganizationId(organization.getParentId()+organization.getId()+",");
            return accountService.save(account) ? RT.ok("保存成功！") : RT.error(RT.ERROR_0101);
        }else{
            return  RT.error(RT.ERROR_0101,"账号已存在！");
        }
    }
    @Resource
    AccountService accountService;
    @Resource
    OrganizationService organizationService;
}

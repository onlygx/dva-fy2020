package com.ow.dva.module.phoneApp.controller;


import com.ow.dva.config.constant.UserConstant;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.account.entity.AccountAdmin;
import com.ow.dva.module.account.service.AccountAdminService;
import com.ow.dva.module.account.service.AccountService;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.base.entity.param.DvaPage;
import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import com.ow.dva.module.permission.entity.AccountRole;
import com.ow.dva.module.permission.service.AccountRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  警员 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-19
 * @version V1.0
 */
@Controller
@Api(tags = "管理员 相关控制器")
@RequestMapping("/police")
public class PoliceController extends BaseController {

    @GetMapping(value = "/treeAdd")
    public String treeAdd(HttpSession session,String parentId, Model model){
        try {
            Account account = (Account) session.getAttribute(UserConstant.ACCOUNT);
            if(account != null && !account.getOrganizationId().equals("")){
                parentId = account.getOrganizationId();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        List<Organization> list = organizationService.findByParentId(parentId);
        model.addAttribute("list",list);
        model.addAttribute("parentId",parentId);
        return "police/tree";
    }

    /**
     * 分页获取用户账号数据列表信息
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "分页获取",notes = "批量获取用户账号信息列表，分页，可携带条件")
    @RequiresPermissions("police:page")
    @GetMapping(value = "/page")
    public String page(HttpSession session,DvaPage<Account> dvaPage, @RequestParam Map<String, Object> param, Model model){
        try {
            Account account = (Account) session.getAttribute(UserConstant.ACCOUNT);
            if(account != null && !account.getOrganizationId().equals("")){
                param.put("organizationId",account.getOrganizationId());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        DvaPage<Account> page = accountService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "police/list";
    }

    /**
     * 进入用户账号添加页面。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入添加页面",notes = "进入用户账号添加页面")
    @RequiresPermissions("police:add")
    @GetMapping(value = "/add")
    public String add(){

        return "police/add";
    }

    /**
     * 添加用户账号信息。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "添加",notes = "添加单个用户账号信息")
    @RequiresPermissions("police:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Account account){
        boolean success = accountService.save(account);
        if(success){
            AccountRole accountRole = new AccountRole();
            accountRole.setAccountId(account.getId());
            accountRole.setRoleId("888887");
            accountRoleService.save(accountRole);
        }
        return success ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
     * 进入用户账号编辑页面。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "进入编辑页面",notes = "进入用户账号编辑页面")
    @RequiresPermissions("police:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",accountService.getById(id));

        return "police/edit";
    }

    /**
     * 修改用户账号信息。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "修改",notes = "修改单个用户账号信息")
    @RequiresPermissions("police:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Account account){

        return accountService.updateById(account) ? RT.ok() : RT.error(1);
    }

    /**
     * 删除单个用户账号信息。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "删除",notes = "删除单个用户账号信息")
    @RequiresPermissions("police:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的用户账号ID") String id){

        return accountService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
     * 批量删除多个用户账号信息。
     * 创建时间：2020-01-19 高祥
     * 修改时间：2020-01-19 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "批量删除",notes = "批量删除多个用户账号信息")
    @RequiresPermissions("police:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的用户账号ID List") @RequestParam List<String> ids){

        return accountService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    AccountService accountService;

    @Resource
    AccountRoleService accountRoleService;

    @Resource
    OrganizationService organizationService;
}

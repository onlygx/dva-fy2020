package com.ow.dva.module.phoneApp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.company.service.CompanyService;
import com.ow.dva.module.shop.entity.Shop;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * appCompany接口专用controller
 * @author YanZengBao
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "appCompany接口专用controller")
@RequestMapping("/appCompany")
public class AppCompanyController extends BaseController {

    @Resource
    CompanyService companyService;

    /**
     * 描述：根据组织ID查询单位
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @GetMapping(value = "/selectCompany")
    @ApiOperation(value = "根据组织ID查询单位",notes = "根据组织ID查询单位")
    @ResponseBody
    public RT selectCompany(@SessionAttribute(value="account") Account account,String organizationId){
        QueryWrapper<Company> wrapper = new QueryWrapper<>();
        if(account.getType().contains("user")){
            wrapper.eq("account_id",account.getId());

        }else{

        }
        if (organizationId != null && !"".equals(organizationId)){
            wrapper.like("organization_id",organizationId);
        }
        List<Company> companyList = companyService.list(wrapper);
        return RT.ok(companyList);
    }


    /**
     * @Author 闫增宝
     * @Description 根据单位ID查询单位详情
     * @Date 2020/2/16 0:15
     * @Param
     */
    @GetMapping(value = "/selectCompanyInfo")
    @ApiOperation(value = "根据单位ID查询单位详情",notes = "根据单位ID查询单位详情")
    @ResponseBody
    public RT selectCompanyInfo(String companyId){
        Company company = companyService.selectCompanyInfo(companyId);
        return RT.ok(company);
    }

    @PostMapping(value = "/deleteCompanyInfo")
    @ApiOperation(value = "根据单位ID删除单位",notes = "根据单位ID删除单位")
    @ResponseBody
    public RT deleteCompanyInfo(String companyId){
        boolean success = companyService.removeById(companyId);
        return success ? RT.ok("删除成功！") : RT.error(RT.ERROR_0101);
    }


    /**
     * 描述：
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @PostMapping(value = "/saveCompanyInfo")
    @ApiOperation(value = "添加单位以及单位相关详细信息",notes = "添加单位以及单位相关详细信息")
    @ResponseBody
    public RT saveCompanyInfo(@SessionAttribute(value="account") Account account ,Company company){
        company.setAccountId(account.getId());
        if(account.getType().contains("user")){
            company.setStatus(0);
        }else{
            company.setStatus(1);
        }
        return companyService.saveCompanyInfo(company) ? RT.ok("保存成功！") : RT.error(RT.ERROR_0101);
    }
    /**
     * 修改单位信息。
     * 创建时间：2020-02-15 高祥
     * 修改时间：2020-02-15 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "审核",notes = "审核单个单位信息")
    @PostMapping("/checkCompany")
    @ResponseBody
    public RT checkCompany(String id){
        return  companyService.update(new UpdateWrapper<Company>().eq("id",id).set("status",1)) ? RT.ok() : RT.error(1);
    }
}

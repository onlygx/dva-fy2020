package com.ow.dva.module.phoneApp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.shop.entity.Shop;
import com.ow.dva.module.shop.service.ShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * appShop接口专用controller
 * @author YanZengBao
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "appShop接口专用controller")
@RequestMapping("/appShop")
public class AppShopController extends BaseController {

    @Resource
    ShopService shopService;


    /**
     * 描述：根据组织ID查询门店
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @RequestMapping(value = "/selectShop")
    @ApiOperation(value = "根据组织ID查询门店",notes = "根据组织ID查询门店")
    @ResponseBody
    public RT selectShop(@SessionAttribute(value="account") Account account,String organizationId){
        QueryWrapper<Shop> wrapper = new QueryWrapper<>();
        if(account.getType().contains("user")){
            wrapper.eq("account_id",account.getId());

        }else{

        }
        if (organizationId != null && !"".equals(organizationId)){
            wrapper.like("organization_id",organizationId);
        }
        List<Shop> shopList = shopService.list(wrapper);
        return RT.ok(shopList);
    }

    @GetMapping(value = "/selectShopInfo")
    @ApiOperation(value = "根据门店ID查询门店详情",notes = "根据门店ID查询门店详情")
    @ResponseBody
    public RT selectShopInfo(String shopId){
        Shop shop = shopService.selectShopInfo(shopId);
        return RT.ok(shop);
    }

    @PostMapping(value = "/deleteShopInfo")
    @ApiOperation(value = "根据门店ID删除门店",notes = "根据门店ID删除门店")
    @ResponseBody
    public RT deleteShopInfo(String shopId){
        boolean success  = shopService.removeById(shopId);
        return success ? RT.ok("删除成功！") : RT.error(RT.ERROR_0101);
    }


    /**
     * 描述：添加门店以及门店相关详细信息
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @PostMapping(value = "/saveShopInfo")
    @ApiOperation(value = "添加门店以及门店相关详细信息",notes = "添加门店以及门店相关详细信息")
    @ResponseBody
    public RT saveShopInfo(@SessionAttribute(value="account") Account account,Shop shop){
        shop.setAccountId(account.getId());
        if(account.getType().contains("user")){

            shop.setStatus(0);
        }else{
            shop.setStatus(1);
        }
        return shopService.saveShopInfo(shop) ? RT.ok("保存成功！") : RT.error(RT.ERROR_0101);
    }

    /**
     * 审核店铺信息。
     * 创建时间：2020-02-15 高祥
     * 修改时间：2020-02-15 高祥 加注释
     * 修改时间：
     */
    @ApiOperation(value = "审核",notes = "审核单个店铺信息")
    @PostMapping("/checkShop")
    @ResponseBody
    public RT checkShop(String id){
        return  shopService.update(new UpdateWrapper<Shop>().eq("id",id).set("status",1)) ? RT.ok() : RT.error(1);
    }
}

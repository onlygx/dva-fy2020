package com.ow.dva.module.phoneApp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.base.controller.BaseController;
import com.ow.dva.module.base.entity.json.RT;
import com.ow.dva.module.room.entity.Building;
import com.ow.dva.module.room.entity.Estate;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.room.entity.Unit;
import com.ow.dva.module.room.service.BuildingService;
import com.ow.dva.module.room.service.EstateService;
import com.ow.dva.module.room.service.RoomService;
import com.ow.dva.module.room.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * appRoom接口专用controller
 * @author YanZengBao
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "appRoom接口专用controller")
@RequestMapping("/appRoom")
public class AppRoomController extends BaseController {


    @Resource
    EstateService estateService;
    @Resource
    BuildingService buildingService;
    @Resource
    UnitService unitService;
    @Resource
    RoomService roomService;


    /**
     * 描述：根据组织ID查询小区
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @GetMapping(value = "/selectEstate")
    @ApiOperation(value = "根据组织ID查询小区",notes = "根据组织ID查询小区")
    @ResponseBody
    public RT selectEstate(String organizationId){

        QueryWrapper<Estate> wrapper = new QueryWrapper<>();
        if (organizationId != null && !"".equals(organizationId)){
            wrapper.like("organization_id",organizationId);
        }
        List<Estate> estateList = estateService.list(wrapper);
        return RT.ok(estateList);
    }


    /**
     * 描述：根据小区ID查询楼栋
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @GetMapping(value = "/selectBuilding")
    @ApiOperation(value = "根据小区ID查询楼栋",notes = "根据小区ID查询楼栋")
    @ResponseBody
    public RT selectBuildingByEstateId(String estateId){
        QueryWrapper<Building> wrapper = new QueryWrapper<>();
        if (estateId != null && !"".equals(estateId)){
            wrapper.eq("estate_id",estateId);
        }
        List<Building> buildingList = buildingService.list(wrapper);
        return RT.ok(buildingList);
    }


    /**
     * 描述：根据楼栋ID查询单元
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @GetMapping(value = "/selectUnit")
    @ApiOperation(value = "根据楼栋ID查询单元",notes = "根据楼栋ID查询单元")
    @ResponseBody
    public RT selectUnitByBuildId(String buildingId){
        QueryWrapper<Unit> wrapper = new QueryWrapper<>();
        if (buildingId != null && !"".equals(buildingId)){
            wrapper.eq("building_id",buildingId);
        }
        List<Unit> unitList = unitService.list(wrapper);
        return RT.ok(unitList);
    }

    /**
     * @Author 闫增宝
     * @Description 根据单元ID查询房屋
     * @Date 2020/2/15 23:08
     * @Param unitId 单元ID
     */
    @GetMapping(value = "/selectRoom")
    @ApiOperation(value = "根据单元ID查询房屋",notes = "根据单元ID查询房屋")
    @ResponseBody
    public RT selectRoomByUnitId(String unitId){
        QueryWrapper<Room> wrapper = new QueryWrapper<>();
        if (unitId != null && !"".equals(unitId)){
            wrapper.eq("unit_id",unitId);
        }
        List<Room> roomList = roomService.list(wrapper);
        return RT.ok(roomList);
    }

    /**
     * @Author 闫增宝
     * @Description 根据房屋ID查询房屋详情
     * @Date 2020/2/15 23:22
     * @Param roomId 房间ID
     */
    @GetMapping(value = "/selectRoomInfo")
    @ApiOperation(value = "根据房屋ID查询房屋详情",notes = "根据房屋ID查询房屋详情")
    @ResponseBody
    public RT selectRoomInfo(String roomId){
        Room room = roomService.selectRoomInfoById(roomId);
        return RT.ok(room);
    }

    @PostMapping(value = "/deleteRoomInfo")
    @ApiOperation(value = "根据房屋ID查询房屋详情",notes = "根据房屋ID查询房屋详情")
    @ResponseBody
    public RT deleteRoomInfo(String roomId){
        boolean success = roomService.removeById(roomId);
        return success ? RT.ok("删除成功！") : RT.error(RT.ERROR_0101);
    }

    /**
     * 描述：添加房屋以及房屋相关详细信息
     * 创建时间： 2020/2/15 YanZengBao
     * 修改时间： 2020/2/15 YanZengBao  加注释
     * 修改时间：
     */
    @PostMapping(value = "/saveRoomInfo")
    @ApiOperation(value = "添加房屋以及房屋相关详细信息",notes = "添加房屋以及房屋相关详细信息")
    @ResponseBody
    public RT saveRoomInfo( Room room){

        //基础信息判断
        if(room == null) return RT.error(1,"房间信息有误！");
        if(room.getName() == null || "".equals(room.getName())) return RT.error(2,"房间号输入错误！");
        if(room.getUnitId() == null || "".equals(room.getUnitId())) return RT.error(3,"单元信息输入错误！");

        //判断房屋号是否已录入
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("name",room.getName());
        queryWrapper.eq("unit_id",room.getUnitId());
        int count = roomService.count(queryWrapper);
        if (count > 0) return  RT.error(2,"房间号已存在，请重新输入！");

        Unit byId = unitService.getById(room.getUnitId());
        if(byId != null){
            room.setCreateTime(new Date());

            room.setOrganizationId(byId.getOrganizationId());
            room.setBuildingId(byId.getBuildingId());
            room.setEstateId(byId.getEstateId());
        }
        return roomService.saveRoomInfo(room) ? RT.ok("保存成功！") : RT.error(RT.ERROR_0101);
    }
}

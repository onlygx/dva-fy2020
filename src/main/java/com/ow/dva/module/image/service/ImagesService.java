package com.ow.dva.module.image.service;

import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.shop.entity.Shop;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GaoXiang
 * @since 2020-02-27
 * @version V1.0
 */
public interface ImagesService extends DvaService<Images> {

    /**
    * 分页获取数据列表信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Images> page(DvaPage<Images> page, Map<String,Object> param);

    void quickSaveImage(String type,String fromId,String url);

    List<Images> selectByTypeFrom(String type,String fromId);

    void saveCompanyImege(Company company);

    void saveShopImege(Shop shop);

    void saveRoomImege(Room room);

    void savePeopleImage(People people);

    void saveCarImage(Car car);

    void savePetImage(Pet pet);
}

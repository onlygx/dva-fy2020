package com.ow.dva.module.image.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ow.dva.module.company.entity.Company;
import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.image.mapper.ImagesMapper;
import com.ow.dva.module.image.service.ImagesService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.room.entity.Room;
import com.ow.dva.module.shop.entity.Shop;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GaoXiang
 * @since 2020-02-27
 * @version V1.0
 */
@Service
public class ImagesServiceImpl extends DvaServiceImpl<ImagesMapper, Images> implements ImagesService {

    @DvaCache5M
    @Override
    public DvaPage<Images> page(DvaPage<Images> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public void quickSaveImage(String type, String fromId, String url) {
        Images images = new Images();
        images.setFromId(fromId);
        images.setType(type);
        images.setUrl(url);
        baseMapper.insert(images);
    }

    @Override
    public List<Images> selectByTypeFrom(String type, String fromId) {
        QueryWrapper<Images> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("from_id",fromId);
        queryWrapper.eq("type",type);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public void saveCompanyImege(Company company) {
        if(company == null || company.getImages() == null){return;}
        String[] images = company.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("company",company.getId(),image);
            }
        }
    }

    @Override
    public void saveShopImege(Shop shop) {
        if(shop == null || shop.getImages() == null){return;}
        String[] images = shop.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("shop",shop.getId(),image);
            }
        }
    }

    @Override
    public void saveRoomImege(Room room) {
        if(room == null || room.getImages() == null){return;}
        String[] images = room.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("room",room.getId(),image);
            }
        }
    }

    @Override
    public void savePeopleImage(People people) {
        if(people == null || people.getImages() == null){return;}
        String[] images = people.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("people",people.getId(),image);
            }
        }
    }

    @Override
    public void saveCarImage(Car car) {
        if(car == null || car.getImages() == null){return;}
        String[] images = car.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("car",car.getId(),image);
            }
        }
    }

    @Override
    public void savePetImage(Pet pet) {
        if(pet == null || pet.getImages() == null){return;}
        String[] images = pet.getImages().split(",");
        for (String image : images) {
            if(!image.equals("")){
                quickSaveImage("pet",pet.getId(),image);
            }
        }
    }
}

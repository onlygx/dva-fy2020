package com.ow.dva.module.image.mapper;

import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GaoXiang
 * @since 2020-02-27
 * @version V1.0
 */
public interface ImagesMapper extends BaseMapper<Images> {

    /**
    * 分页获取数据列表信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Images> page(DvaPage<Images> page, @Param("param") Map<String, Object> param);
}

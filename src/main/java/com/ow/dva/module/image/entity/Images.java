package com.ow.dva.module.image.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author GaoXiang
 * @since 2020-02-27
 * @version V1.0
 */
@TableName("dva_images")
@ApiModel(value="Images对象", description="")
public class Images extends BaseEntity<Images> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = " ")
    private String id;

    private String url;

    private String type;

    private String fromId;

    @TableField("createTime")
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Images{" +
            "id=" + id +
            ", url=" + url +
            ", type=" + type +
            ", fromId=" + fromId +
            ", createTime=" + createTime +
        "}";
    }
}

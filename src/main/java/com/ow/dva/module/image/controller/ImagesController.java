package com.ow.dva.module.image.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.image.entity.Images;
import com.ow.dva.module.image.service.ImagesService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author GaoXiang
 * @since 2020-02-27
 * @version V1.0
 */
@Controller
@Api(tags = " 相关控制器")
@RequestMapping("/images")
public class ImagesController extends BaseController {

    /**
    * 分页获取数据列表信息
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取信息列表，分页，可携带条件")
    @RequiresPermissions("images:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Images> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Images> page = imagesService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "images/list";
    }

    /**
    * 进入添加页面。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入添加页面")
    @RequiresPermissions("images:add")
    @GetMapping(value = "/add")
    public String add(){

        return "images/add";
    }

    /**
    * 添加信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个信息")
    @RequiresPermissions("images:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Images images){

        return imagesService.save(images) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入编辑页面。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入编辑页面")
    @RequiresPermissions("images:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",imagesService.getById(id));

        return "images/edit";
    }

    /**
    * 修改信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个信息")
    @RequiresPermissions("images:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Images images){

        return imagesService.updateById(images) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个信息")
    @RequiresPermissions("images:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的ID") String id){

        return imagesService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个信息。
    * 创建时间：2020-02-27 GaoXiang
    * 修改时间：2020-02-27 GaoXiang 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个信息")
    @RequiresPermissions("images:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的ID List") @RequestParam List<String> ids){

        return imagesService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    ImagesService imagesService;
}

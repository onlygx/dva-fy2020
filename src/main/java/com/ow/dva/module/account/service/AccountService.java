package com.ow.dva.module.account.service;

import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
public interface AccountService extends DvaService<Account> {

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-09 高祥
    * 修改时间：2020-01-09 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Account> page(DvaPage<Account> page, Map<String,Object> param);

    Account findByUserName(String userName);
}

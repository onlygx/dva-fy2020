package com.ow.dva.module.account.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.account.mapper.AccountMapper;
import com.ow.dva.module.account.service.AccountService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class AccountServiceImpl extends DvaServiceImpl<AccountMapper, Account> implements AccountService {

    @DvaCache5M
    @Override
    public DvaPage<Account> page(DvaPage<Account> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public Account findByUserName(String userName) {
        
        return baseMapper.findByUserName(userName);
    }
}

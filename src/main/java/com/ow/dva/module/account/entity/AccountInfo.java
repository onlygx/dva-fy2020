package com.ow.dva.module.account.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@TableName("dva_account_info")
@ApiModel(value="AccountInfo对象", description="")
public class AccountInfo extends BaseEntity<AccountInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "所属account的ID")
    private String accountId;

    @ApiModelProperty(value = "用户姓名")
    private String name;

    @ApiModelProperty(value = "用户身份证")
    private String idCard;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "用户电话")
    private String phone;

    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @ApiModelProperty(value = "用户地址")
    private String address;

    @ApiModelProperty(value = "用户性别")
    private String sex;

    @ApiModelProperty(value = "实名信息是否通过审核：1通过，0未通过")
    private Integer cardVerify;

    @TableField(exist = false)
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    public Integer getCardVerify() {
        return cardVerify;
    }

    public void setCardVerify(Integer cardVerify) {
        this.cardVerify = cardVerify;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
            "id=" + id +
            ", accountId=" + accountId +
            ", name=" + name +
            ", idCard=" + idCard +
            ", createTime=" + createTime +
            ", phone=" + phone +
            ", email=" + email +
            ", address=" + address +
            ", sex=" + sex +
            ", cardVerify=" + cardVerify +
        "}";
    }
}

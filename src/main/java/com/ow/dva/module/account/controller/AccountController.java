package com.ow.dva.module.account.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.account.entity.Account;
import com.ow.dva.module.account.service.AccountService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户账号 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-19
 * @version V1.0
 */
@Controller
@Api(tags = "用户账号 相关控制器")
@RequestMapping("/account")
public class AccountController extends BaseController {

    /**
    * 分页获取用户账号数据列表信息
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取用户账号信息列表，分页，可携带条件")
    @RequiresPermissions("account:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Account> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Account> page = accountService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "account/list";
    }

    /**
    * 进入用户账号添加页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入用户账号添加页面")
    @RequiresPermissions("account:add")
    @GetMapping(value = "/add")
    public String add(){

        return "account/add";
    }

    /**
    * 添加用户账号信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个用户账号信息")
    @RequiresPermissions("account:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Account account){

        return accountService.save(account) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入用户账号编辑页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入用户账号编辑页面")
    @RequiresPermissions("account:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",accountService.getById(id));

        return "account/edit";
    }

    /**
    * 修改用户账号信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个用户账号信息")
    @RequiresPermissions("account:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Account account){

        return accountService.updateById(account) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个用户账号信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个用户账号信息")
    @RequiresPermissions("account:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的用户账号ID") String id){

        return accountService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个用户账号信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个用户账号信息")
    @RequiresPermissions("account:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的用户账号ID List") @RequestParam List<String> ids){

        return accountService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    AccountService accountService;
}

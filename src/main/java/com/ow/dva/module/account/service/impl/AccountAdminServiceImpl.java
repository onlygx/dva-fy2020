package com.ow.dva.module.account.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.account.entity.AccountAdmin;
import com.ow.dva.module.account.mapper.AccountAdminMapper;
import com.ow.dva.module.account.service.AccountAdminService;
import com.ow.dva.module.account.service.AccountInfoService;
import com.ow.dva.module.account.service.AccountService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class AccountAdminServiceImpl extends DvaServiceImpl<AccountAdminMapper, AccountAdmin> implements AccountAdminService {

    @DvaCache5M
    @Override
    public DvaPage<AccountAdmin> page(DvaPage<AccountAdmin> page, Map<String, Object> param) {
        DvaPage<AccountAdmin> dvaPage = baseMapper.page(page,param);
        dvaPage.getRecords().forEach(accountAdmin -> {
            accountAdmin.setAccount(accountService.getById(accountAdmin.getAccountId()));
            accountAdmin.setAccountInfo(accountInfoService.findByAccountId(accountAdmin.getAccountId()));
        });
        return dvaPage;
    }

    @Override
    public AccountAdmin findByAccountId(String accountId) {
        return baseMapper.findByAccountId(accountId);
    }

    @Resource
    private AccountService accountService;

    @Resource
    private AccountInfoService accountInfoService;
}

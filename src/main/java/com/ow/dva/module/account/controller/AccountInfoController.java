package com.ow.dva.module.account.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.account.entity.AccountInfo;
import com.ow.dva.module.account.service.AccountInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 账号详情 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-19
 * @version V1.0
 */
@Controller
@Api(tags = "账号详情 相关控制器")
@RequestMapping("/accountInfo")
public class AccountInfoController extends BaseController {

    /**
    * 分页获取账号详情数据列表信息
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取账号详情信息列表，分页，可携带条件")
    @RequiresPermissions("accountInfo:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<AccountInfo> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<AccountInfo> page = accountInfoService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "accountInfo/list";
    }

    /**
    * 进入账号详情添加页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入账号详情添加页面")
    @RequiresPermissions("accountInfo:add")
    @GetMapping(value = "/add")
    public String add(){

        return "accountInfo/add";
    }

    /**
    * 添加账号详情信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个账号详情信息")
    @RequiresPermissions("accountInfo:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(AccountInfo accountInfo){

        return accountInfoService.save(accountInfo) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入账号详情编辑页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入账号详情编辑页面")
    @RequiresPermissions("accountInfo:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",accountInfoService.getById(id));

        return "accountInfo/edit";
    }

    /**
    * 修改账号详情信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个账号详情信息")
    @RequiresPermissions("accountInfo:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(AccountInfo accountInfo){

        return accountInfoService.updateById(accountInfo) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个账号详情信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个账号详情信息")
    @RequiresPermissions("accountInfo:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的账号详情ID") String id){

        return accountInfoService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个账号详情信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个账号详情信息")
    @RequiresPermissions("accountInfo:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的账号详情ID List") @RequestParam List<String> ids){

        return accountInfoService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    AccountInfoService accountInfoService;
}

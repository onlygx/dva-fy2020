package com.ow.dva.module.account.controller;


import com.ow.dva.module.organization.entity.Organization;
import com.ow.dva.module.organization.service.OrganizationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.account.entity.AccountAdmin;
import com.ow.dva.module.account.service.AccountAdminService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 管理员 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-01-19
 * @version V1.0
 */
@Controller
@Api(tags = "管理员 相关控制器")
@RequestMapping("/accountAdmin")
public class AccountAdminController extends BaseController {


    @GetMapping(value = "/treeAdd")
    public String treeAdd(String parentId, Model model){
        List<Organization> list = organizationService.findByParentId(parentId);
        model.addAttribute("list",list);
        model.addAttribute("parentId",parentId);
        return "accountAdmin/tree";
    }

    /**
    * 分页获取管理员数据列表信息
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取管理员信息列表，分页，可携带条件")
    @RequiresPermissions("accountAdmin:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<AccountAdmin> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<AccountAdmin> page = accountAdminService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "accountAdmin/list";
    }

    /**
    * 进入管理员添加页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入管理员添加页面")
    @RequiresPermissions("accountAdmin:add")
    @GetMapping(value = "/add")
    public String add(){

        return "accountAdmin/add";
    }

    /**
    * 添加管理员信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个管理员信息")
    @RequiresPermissions("accountAdmin:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(AccountAdmin accountAdmin){

        return accountAdminService.save(accountAdmin) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入管理员编辑页面。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入管理员编辑页面")
    @RequiresPermissions("accountAdmin:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",accountAdminService.getById(id));

        return "accountAdmin/edit";
    }

    /**
    * 修改管理员信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个管理员信息")
    @RequiresPermissions("accountAdmin:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(AccountAdmin accountAdmin){

        return accountAdminService.updateById(accountAdmin) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个管理员信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个管理员信息")
    @RequiresPermissions("accountAdmin:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的管理员ID") String id){

        return accountAdminService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个管理员信息。
    * 创建时间：2020-01-19 高祥
    * 修改时间：2020-01-19 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个管理员信息")
    @RequiresPermissions("accountAdmin:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的管理员ID List") @RequestParam List<String> ids){

        return accountAdminService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    AccountAdminService accountAdminService;

    @Resource
    OrganizationService organizationService;
}

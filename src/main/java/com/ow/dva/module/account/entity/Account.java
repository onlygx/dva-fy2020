package com.ow.dva.module.account.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@TableName("dva_account")
@ApiModel(value="Account对象", description="")
public class Account extends BaseEntity<Account> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "昵称")
    private String nick;

    @ApiModelProperty(value = "用户头像")
    private String head;

    @ApiModelProperty(value = "用户标签：user,admin,shop等,默认user,")
    private String type;

    @ApiModelProperty(value = "用户状态：1正常，-1被封，0禁止登录")
    private Integer status;

    @ApiModelProperty(value = "qq快捷登录")
    private String qqId;

    @ApiModelProperty(value = "微信快捷登录")
    private String wxId;

    @ApiModelProperty(value = "组织ID")
    private String organizationId;

    @TableField(exist = false)
    private AccountInfo accountInfo;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getQqId() {
        return qqId;
    }

    public void setQqId(String qqId) {
        this.qqId = qqId;
    }
    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Account{" +
            "id=" + id +
            ", userName=" + userName +
            ", password=" + password +
            ", nick=" + nick +
            ", head=" + head +
            ", type=" + type +
            ", status=" + status +
            ", qqId=" + qqId +
            ", wxId=" + wxId +
        "}";
    }
}

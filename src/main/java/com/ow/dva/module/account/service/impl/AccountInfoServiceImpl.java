package com.ow.dva.module.account.service.impl;

import com.ow.dva.config.annotation.DvaCache5M;
import com.ow.dva.module.account.entity.AccountInfo;
import com.ow.dva.module.account.mapper.AccountInfoMapper;
import com.ow.dva.module.account.service.AccountInfoService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-01-09
 * @version V1.0
 */
@Service
public class AccountInfoServiceImpl extends DvaServiceImpl<AccountInfoMapper, AccountInfo> implements AccountInfoService {

    @DvaCache5M
    @Override
    public DvaPage<AccountInfo> page(DvaPage<AccountInfo> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }

    @Override
    public AccountInfo findByAccountId(String accountId) {
        return baseMapper.findByAccountId(accountId);
    }
}

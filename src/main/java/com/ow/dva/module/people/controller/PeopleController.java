package com.ow.dva.module.people.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.service.PeopleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 人员 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "人员 相关控制器")
@RequestMapping("/people")
public class PeopleController extends BaseController {

    /**
    * 分页获取人员数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取人员信息列表，分页，可携带条件")
    @RequiresPermissions("people:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<People> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<People> page = peopleService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "people/list";
    }

    /**
    * 进入人员添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入人员添加页面")
    @RequiresPermissions("people:add")
    @GetMapping(value = "/add")
    public String add(){

        return "people/add";
    }

    /**
    * 添加人员信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个人员信息")
    @RequiresPermissions("people:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(People people){

        return peopleService.save(people) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入人员编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入人员编辑页面")
    @RequiresPermissions("people:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",peopleService.getById(id));

        return "people/edit";
    }

    /**
    * 修改人员信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个人员信息")
    @RequiresPermissions("people:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(People people){

        return peopleService.updateById(people) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个人员信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个人员信息")
    @RequiresPermissions("people:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的人员ID") String id){

        return peopleService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个人员信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个人员信息")
    @RequiresPermissions("people:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的人员ID List") @RequestParam List<String> ids){

        return peopleService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    PeopleService peopleService;
}

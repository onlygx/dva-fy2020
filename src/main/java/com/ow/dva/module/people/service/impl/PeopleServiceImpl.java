package com.ow.dva.module.people.service.impl;

import com.ow.dva.module.people.entity.People;
import com.ow.dva.module.people.mapper.PeopleMapper;
import com.ow.dva.module.people.service.PeopleService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 人员 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class PeopleServiceImpl extends DvaServiceImpl<PeopleMapper, People> implements PeopleService {

    @DvaCache5M
    @Override
    public DvaPage<People> page(DvaPage<People> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }
}

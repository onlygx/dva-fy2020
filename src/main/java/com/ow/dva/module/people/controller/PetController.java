package com.ow.dva.module.people.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.people.service.PetService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 宠物 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "宠物 相关控制器")
@RequestMapping("/pet")
public class PetController extends BaseController {

    /**
    * 分页获取宠物数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取宠物信息列表，分页，可携带条件")
    @RequiresPermissions("pet:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Pet> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Pet> page = petService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "pet/list";
    }

    /**
    * 进入宠物添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入宠物添加页面")
    @RequiresPermissions("pet:add")
    @GetMapping(value = "/add")
    public String add(){

        return "pet/add";
    }

    /**
    * 添加宠物信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个宠物信息")
    @RequiresPermissions("pet:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Pet pet){

        return petService.save(pet) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入宠物编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入宠物编辑页面")
    @RequiresPermissions("pet:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",petService.getById(id));

        return "pet/edit";
    }

    /**
    * 修改宠物信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个宠物信息")
    @RequiresPermissions("pet:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Pet pet){

        return petService.updateById(pet) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个宠物信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个宠物信息")
    @RequiresPermissions("pet:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的宠物ID") String id){

        return petService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个宠物信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个宠物信息")
    @RequiresPermissions("pet:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的宠物ID List") @RequestParam List<String> ids){

        return petService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    PetService petService;
}

package com.ow.dva.module.people.service.impl;

import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.mapper.CarMapper;
import com.ow.dva.module.people.service.CarService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 车辆 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class CarServiceImpl extends DvaServiceImpl<CarMapper, Car> implements CarService {

    @DvaCache5M
    @Override
    public DvaPage<Car> page(DvaPage<Car> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }
}

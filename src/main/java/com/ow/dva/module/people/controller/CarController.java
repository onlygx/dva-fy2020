package com.ow.dva.module.people.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.ow.dva.module.base.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.people.service.CarService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;
import com.ow.dva.module.base.entity.json.RT;
import org.springframework.ui.Model;
import javax.annotation.Resource;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 车辆 前端控制器
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Controller
@Api(tags = "车辆 相关控制器")
@RequestMapping("/car")
public class CarController extends BaseController {

    /**
    * 分页获取车辆数据列表信息
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "分页获取",notes = "批量获取车辆信息列表，分页，可携带条件")
    @RequiresPermissions("car:page")
    @GetMapping(value = "/page")
    public String page(DvaPage<Car> dvaPage, @RequestParam Map<String, Object> param, Model model){

        DvaPage<Car> page = carService.page(dvaPage,param);
        model.addAttribute("page",page);
        model.addAttribute("param",param);
        return "car/list";
    }

    /**
    * 进入车辆添加页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入添加页面",notes = "进入车辆添加页面")
    @RequiresPermissions("car:add")
    @GetMapping(value = "/add")
    public String add(){

        return "car/add";
    }

    /**
    * 添加车辆信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "添加",notes = "添加单个车辆信息")
    @RequiresPermissions("car:add")
    @PostMapping("/save")
    @ResponseBody
    public RT save(Car car){

        return carService.save(car) ? RT.ok() : RT.error(RT.ERROR_0101);
    }

    /**
    * 进入车辆编辑页面。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "进入编辑页面",notes = "进入车辆编辑页面")
    @RequiresPermissions("car:edit")
    @GetMapping(value = "/edit")
    public String edit(String id,Model model){

        model.addAttribute("data",carService.getById(id));

        return "car/edit";
    }

    /**
    * 修改车辆信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "修改",notes = "修改单个车辆信息")
    @RequiresPermissions("car:edit")
    @PostMapping("/update")
    @ResponseBody
    public RT update(Car car){

        return carService.updateById(car) ? RT.ok() : RT.error(1);
    }

    /**
    * 删除单个车辆信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "删除",notes = "删除单个车辆信息")
    @RequiresPermissions("car:delete")
    @PostMapping("/delete")
    @ResponseBody
    public RT delete(@ApiParam(name="要删除的车辆ID") String id){

        return carService.removeById(id) ? RT.ok() : RT.error(1);
    }

    /**
    * 批量删除多个车辆信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    */
    @ApiOperation(value = "批量删除",notes = "批量删除多个车辆信息")
    @RequiresPermissions("car:delete")
    @PostMapping("/deleteByIds")
    @ResponseBody
    public RT deleteByIds(@ApiParam(name="要删除的车辆ID List") @RequestParam List<String> ids){

        return carService.removeByIds(ids) ? RT.ok() : RT.error(1);
    }


    @Resource
    CarService carService;
}

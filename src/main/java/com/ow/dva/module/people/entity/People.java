package com.ow.dva.module.people.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 人员
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_people")
@ApiModel(value="People对象", description="人员")
public class People extends BaseEntity<People> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "身份证")
    private String card;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "户籍")
    private String address;

    @ApiModelProperty(value = "工作/职业")
    private String work;

    @ApiModelProperty(value = "是否有居住证")
    private String jzz;

    @ApiModelProperty(value = "居住证编号")
    private String jzzNumber;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "职务/身份")
    private String identity;

    @ApiModelProperty(value = "数据关联来源")
    private String fromType;

    @ApiModelProperty(value = "来源IDS")
    private String fromId;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @TableField(exist = false)
    private String images;

    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
    public String getJzz() {
        return jzz;
    }

    public void setJzz(String jzz) {
        this.jzz = jzz;
    }
    public String getJzzNumber() {
        return jzzNumber;
    }

    public void setJzzNumber(String jzzNumber) {
        this.jzzNumber = jzzNumber;
    }
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }
    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }
    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "People{" +
            "id=" + id +
            ", name=" + name +
            ", card=" + card +
            ", phone=" + phone +
            ", address=" + address +
            ", work=" + work +
            ", jzz=" + jzz +
            ", jzzNumber=" + jzzNumber +
            ", sex=" + sex +
            ", identity=" + identity +
            ", fromType=" + fromType +
            ", fromId=" + fromId +
            ", organizationId=" + organizationId +
        "}";
    }
}

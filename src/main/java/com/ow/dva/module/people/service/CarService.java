package com.ow.dva.module.people.service;

import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.base.service.DvaService;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 车辆 服务类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface CarService extends DvaService<Car> {

    /**
    * 分页获取车辆数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Car> page(DvaPage<Car> page, Map<String,Object> param);
}

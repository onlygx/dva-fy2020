package com.ow.dva.module.people.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 车辆
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_car")
@ApiModel(value="Car对象", description="车辆")
public class Car extends BaseEntity<Car> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "车牌号")
    private String number;

    @ApiModelProperty(value = "车辆类型")
    private String type;

    @ApiModelProperty(value = "车辆颜色")
    private String color;

    @ApiModelProperty(value = "品牌")
    private String brand;

    @ApiModelProperty(value = "是否是租车")
    private String chuzu;

    @ApiModelProperty(value = "车主")
    private String owner;

    @ApiModelProperty(value = "车主身份证")
    private String ownerCard;

    @ApiModelProperty(value = "车主手机号")
    private String ownerPhone;

    @ApiModelProperty(value = "驾驶人")
    private String driver;

    @ApiModelProperty(value = "驾驶人身份证")
    private String driverCard;

    @ApiModelProperty(value = "驾驶人手机号")
    private String driverPhone;

    @ApiModelProperty(value = "数据关联来源")
    private String fromType;

    @ApiModelProperty(value = "来源IDS")
    private String fromId;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @TableField(exist = false)
    private String images;

    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getOwnerCard() {
        return ownerCard;
    }

    public void setOwnerCard(String ownerCard) {
        this.ownerCard = ownerCard;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDriverCard() {
        return driverCard;
    }

    public void setDriverCard(String driverCard) {
        this.driverCard = driverCard;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getChuzu() {
        return chuzu;
    }

    public void setChuzu(String chuzu) {
        this.chuzu = chuzu;
    }
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }
    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + id + '\'' +
                ", number='" + number + '\'' +
                ", type='" + type + '\'' +
                ", color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                ", chuzu='" + chuzu + '\'' +
                ", owner='" + owner + '\'' +
                ", ownerCard='" + ownerCard + '\'' +
                ", ownerPhone='" + ownerPhone + '\'' +
                ", driver='" + driver + '\'' +
                ", driverCard='" + driverCard + '\'' +
                ", driverPhone='" + driverPhone + '\'' +
                ", fromType='" + fromType + '\'' +
                ", fromId='" + fromId + '\'' +
                ", organizationId='" + organizationId + '\'' +
                '}';
    }
}

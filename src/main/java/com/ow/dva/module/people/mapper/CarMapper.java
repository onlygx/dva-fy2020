package com.ow.dva.module.people.mapper;

import com.ow.dva.module.people.entity.Car;
import com.ow.dva.module.base.mapper.BaseMapper;
import com.ow.dva.module.base.entity.param.DvaPage;
import org.apache.ibatis.annotations.Param;
import java.util.Map;

/**
 * <p>
 * 车辆 Mapper 接口
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
public interface CarMapper extends BaseMapper<Car> {

    /**
    * 分页获取车辆数据列表信息。
    * 创建时间：2020-02-15 高祥
    * 修改时间：2020-02-15 高祥 加注释
    * 修改时间：
    * @param page 分页参数
    * @param param 条件参数
    * @return 带分页的数据集
    */
    DvaPage<Car> page(DvaPage<Car> page, @Param("param") Map<String, Object> param);
}

package com.ow.dva.module.people.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ow.dva.module.base.entity.base.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import com.ow.dva.module.image.entity.Images;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 宠物
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@TableName("dva_pet")
@ApiModel(value="Pet对象", description="宠物")
public class Pet extends BaseEntity<Pet> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据ID")
    private String id;

    @ApiModelProperty(value = "品种")
    private String variety;

    @ApiModelProperty(value = "颜色")
    private String color;

    @ApiModelProperty(value = "大小")
    private String size;

    @ApiModelProperty(value = "是否有证件")
    private String card;

    @ApiModelProperty(value = "所有人")
    private String owner;

    @ApiModelProperty(value = "数据关联来源")
    private String fromType;

    @ApiModelProperty(value = "来源IDS")
    private String fromId;

    @ApiModelProperty(value = "所属组织")
    private String organizationId;

    @TableField(exist = false)
    private String images;

    @TableField(exist = false)
    private List<Images> imageList;

    public List<Images> getImageList() {
        return imageList;
    }

    public void setImageList(List<Images> imageList) {
        this.imageList = imageList;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }
    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Pet{" +
            "id=" + id +
            ", variety=" + variety +
            ", color=" + color +
            ", size=" + size +
            ", card=" + card +
            ", owner=" + owner +
            ", fromType=" + fromType +
            ", fromId=" + fromId +
            ", organizationId=" + organizationId +
        "}";
    }
}

package com.ow.dva.module.people.service.impl;

import com.ow.dva.module.people.entity.Pet;
import com.ow.dva.module.people.mapper.PetMapper;
import com.ow.dva.module.people.service.PetService;
import com.ow.dva.module.base.service.impl.DvaServiceImpl;
import com.ow.dva.config.annotation.DvaCache5M;
import org.springframework.stereotype.Service;
import com.ow.dva.module.base.entity.param.DvaPage;
import java.util.Map;

/**
 * <p>
 * 宠物 服务实现类
 * </p>
 *
 * @author 高祥
 * @since 2020-02-15
 * @version V1.0
 */
@Service
public class PetServiceImpl extends DvaServiceImpl<PetMapper, Pet> implements PetService {

    @DvaCache5M
    @Override
    public DvaPage<Pet> page(DvaPage<Pet> page, Map<String, Object> param) {

        return baseMapper.page(page,param);
    }
}

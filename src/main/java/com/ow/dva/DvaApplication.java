package com.ow.dva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DvaApplication {

    //static Logger logger = LoggerFactory.getLogger("Dva");

    public static void main(String[] args) {
        SpringApplication.run(DvaApplication.class, args);
    }
}

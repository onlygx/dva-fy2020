package com.ow.dva.generator;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述 :
 * 包名 : com.ow.dva.generator
 * 作者 : 高祥
 * 时间 : 20-1-6
 * 版本 : V1.0
 */
public class DvaGenerator {

    public static void main(String[] args) {
        generator("GaoXiang","image","dva_",
                "dva_images");
    }

    /**
     * 生成 controller，service,mapper
     * @param author 执行人名称
     * @param module 模块名称 必须英文
     * @param tablePrefix 表前缀 有下划线的带 ‘_’
     * @param tableName 表名，可以写多个，如果不写，则生成表前缀匹配到的所有表
     */
    public static void generator(String author,String module,String tablePrefix,String... tableName){

        // 数据库信息
        String dbUrl = "jdbc:mariadb://onlygx.320.io:3506/dva-fy2020" +
                "?useUnicode=true&characterEncoding=utf-8";
        String dbDriver = "org.mariadb.jdbc.Driver";
        String dbUser = "ruitu";
        String dbPassword = "Ruitu110";

        // 项目路径信息
        String projectPath = System.getProperty("user.dir");
        String sourcePath = "/src/main/java";
        String sourcePackage = "com.ow.dva.module";

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        // 生成文件的输出目录
        gc.setOutputDir(projectPath + sourcePath);
        // 是否覆盖已有文件
        gc.setFileOverride(false);
        // 作者
        gc.setAuthor(author);
        // 是否打开输出目录
        gc.setOpen(false);
        // 开启 swagger2 模式
        gc.setSwagger2(true);
        // 开启 ActiveRecord 模式
        gc.setActiveRecord(true);
        // 开启 BaseResultMap
        //gc.setBaseResultMap(false);
        // 开启 baseColumnList
        gc.setBaseColumnList(false);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        // 生成器设置全局配置
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        // 配置数据库类型
        dsc.setDbType(DbType.MARIADB);
        // 配置数据库连接URL￼
        dsc.setUrl(dbUrl);
        // 配置数据库连接驱动
        dsc.setDriverName(dbDriver);
        // 配置数据库帐号
        dsc.setUsername(dbUser);
        // 配置数据库帐号对应的密码
        dsc.setPassword(dbPassword);
        dsc.setTypeConvert(new DvaTypeConvert());
        // 生成器设置数据源
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        // 父包名 如果为空，将下面子包名必须写全部， 否则就只需写子包名
        pc.setParent(sourcePackage);
        // 父包模块名
        pc.setModuleName(module);
        // 路径配置信息
        //pc.setPathInfo();
        // 生成器设置包信息
        mpg.setPackageInfo(pc);

        // 模板配置(不指定路径则使用项目/templates/下的模板)
        TemplateConfig tc = new TemplateConfig();
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        tc.setEntity("/generator/entity.java");
        tc.setController("/generator/controller.java");
        tc.setService("/generator/service.java");
        tc.setServiceImpl("/generator/serviceImpl.java");
        tc.setMapper("/generator/mapper.java");
        //不自动生成xml 后续手动指定生成位置
        tc.setXml(null);
        // 生成器设置模板
        mpg.setTemplate(tc);

        // 生成器设置模板引擎
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        //自定义属性注入
        InjectionConfig injectionConfig = new InjectionConfig() {
            //在.ftl(或者是.vm)模板中，通过${cfg.abc}获取属性
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("dvaName", "哈哈哈");
                this.setMap(map);
            }
        };
        // 自定义输出路径 将指定模板输出到指定位置
        List<FileOutConfig> focList = new ArrayList<>();
        //手动生成 Mapper.xml 指定生成位置不需要分文件夹
        focList.add(new FileOutConfig("/generator/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getXmlName() + StringPool.DOT_XML;
            }
        });
        //手动生成 list.html 需要分文件夹
        focList.add(new FileOutConfig("/generator/list.html.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String entityName = StrUtil.lowerFirst(tableInfo.getEntityName());
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/templates/" + entityName
                        + "/list.html";
            }
        });
        //手动生成 add.html 需要分文件夹
        focList.add(new FileOutConfig("/generator/add.html.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String entityName = StrUtil.lowerFirst(tableInfo.getEntityName());
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/templates/" + entityName
                        + "/add.html";
            }
        });
        //手动生成 edit.html 需要分文件夹
        focList.add(new FileOutConfig("/generator/edit.html.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                String entityName = StrUtil.lowerFirst(tableInfo.getEntityName());
                // 自定义输入文件名称
                return projectPath + "/src/main/resources/templates/" + entityName
                        + "/edit.html";
            }
        });
        injectionConfig.setFileOutConfigList(focList);
        mpg.setCfg(injectionConfig);


        // 表生成策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 是否跳过视图
        strategy.setSkipView(true);
        // 数据库表映射到实体的命名策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
        //strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // 设置表前缀
        strategy.setTablePrefix(tablePrefix);
        // 设置字段前缀
        //strategy.setFieldPrefix("");
        // 设置自定义继承的Controller类全称，带包名
        strategy.setSuperControllerClass("com.ow.dva.module.base.controller.BaseController");
        // 设置实体父类 自定义继承的Entity类全称，带包名
        strategy.setSuperEntityClass("com.ow.dva.module.base.entity.base.BaseEntity");
        // 自定义基础的Entity类，公共字段
        //strategy.setSuperEntityColumns("id");
        // 自定义继承的Service类全称，带包名
        strategy.setSuperServiceClass("com.ow.dva.module.base.service.DvaService");
        // 自定义继承的ServiceImpl类全称，带包名
        strategy.setSuperServiceImplClass("com.ow.dva.module.base.service.impl.DvaServiceImpl");
        // 自定义继承的Mapper类全称，带包名
        strategy.setSuperMapperClass("com.ow.dva.module.base.mapper.BaseMapper");
        // 生成 @RestController 控制器
        strategy.setRestControllerStyle(false);
        // 需要包含的表名
        strategy.setInclude(tableName);
        // 驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        // 生成器设置表配置
        mpg.setStrategy(strategy);

        // 生成器执行
        mpg.execute();

    }
}

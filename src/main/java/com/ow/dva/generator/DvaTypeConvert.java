package com.ow.dva.generator;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;

/**
 * <p>
 *  数据块字段自定义转换类
 * </p>
 *
 * @author 高祥
 * @version V1.0
 * @since 2020/1/14
 */
public class DvaTypeConvert extends MySqlTypeConvert {

    public DvaTypeConvert(){
        super();
    }

    @Override
    public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
        //将数据库中datetime转换成date
        if ( fieldType.toLowerCase().contains( "datetime" ) ) {
            return DbColumnType.DATE;
        }
        return super.processTypeConvert(globalConfig, fieldType);
    }
}
